modules.define('cart-table', ['i-bem-dom', 'events__channels', 'i-localstorage', 'button', 'modal', 'BEMHTML'],
	function(provide, bemDom, channels, iLocalstorage, Button, Modal, BEMHTML) {

provide(bemDom.declBlock(this.name, {

  	onSetMod: {
        js: {
          	inited: function() {}
        }
    },

    render: function(cartData) {
        bemDom.update(this.domElem, BEMHTML.apply({
            block: 'cart-table',
            elem: 'content',
            cartData: cartData,
            offers: this.params.offers
        }));

        this._bindEvents();
    },

    _bindEvents: function() {
        this._domEvents('count-plus').on('pointerclick', this._onPlusClick, this);
        this._domEvents('count-minus').on('pointerclick', this._onMinusClick, this);
        this._domEvents('bottom-count').on('pointerclick', this._onRemoveClick, this);
    },

    _onPlusClick: function(e) {
        var rowData = this._getRowData(e.bemTarget);

        rowData.cartData = rowData.cartData ? JSON.parse(rowData.cartData) : {};

        if (rowData.cartData[rowData.offerId]) {
            rowData.cartData[rowData.offerId]++;
        }

        iLocalstorage.setItem('cart', JSON.stringify(rowData.cartData));

        bemDom.update(rowData.elem.findChildElem('count').domElem, rowData.cartData[rowData.offerId]);
        bemDom.update(rowData.elem.findChildElem('sum-price').domElem, rowData.offerData.price * rowData.cartData[rowData.offerId] + ' руб');
        rowData.elem.findChildElem('count-minus').delMod('disabled');

        channels('cart').emit('change');
    },

    _onMinusClick: function(e) {
        var rowData = this._getRowData(e.bemTarget);

        rowData.cartData = rowData.cartData ? JSON.parse(rowData.cartData) : {};

        if (rowData.cartData[rowData.offerId]) {
            rowData.cartData[rowData.offerId]--;
        }

        iLocalstorage.setItem('cart', JSON.stringify(rowData.cartData));

        bemDom.update(rowData.elem.findChildElem('count').domElem, rowData.cartData[rowData.offerId]);
        bemDom.update(rowData.elem.findChildElem('sum-price').domElem, rowData.offerData.price * rowData.cartData[rowData.offerId] + ' руб');
        if (rowData.cartData[rowData.offerId] === 1) {
            e.bemTarget.setMod('disabled', true);
        }

        channels('cart').emit('change');
    },

    _onRemoveClick: function(e) {
        var rowData = this._getRowData(e.bemTarget);

        rowData.cartData = rowData.cartData ? JSON.parse(rowData.cartData) : {};

        delete rowData.cartData[rowData.offerId];

        iLocalstorage.setItem('cart', JSON.stringify(rowData.cartData));

        bemDom.destruct(rowData.elem.domElem);

        channels('cart').emit('change');
    },

    _getRowData: function(elem) {
        var offerRow = elem.findParentElem('table-row');
        var offerId = offerRow.params.id;
        var cartData = iLocalstorage.getItem('cart');
        var offerData = this.params.offers.filter(function(item) {
            return item._id.toString() === offerId;
        })[0];

        return {
            elem: offerRow,
            offerId: offerId,
            offerData: offerData,
            cartData: cartData,
        };
    }

}));

});
