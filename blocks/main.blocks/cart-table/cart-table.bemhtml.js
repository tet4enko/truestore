block('cart-table')(

	js()(function() {
		return { offers: this.ctx.offers };
	}),

	elem('content').content()(function() {
		var cartData = this.ctx.cartData;
		var offers = this.ctx.offers;
		var cartItems = [];

		Object.keys(cartData).forEach(function(offerId) {
			var offerData = offers.filter(function(offer) {
				return offer._id.toString() === offerId.toString();
			})[0];

			if (offerData) {
				offerData.count = cartData[offerId];
			}

			cartItems.push(offerData);
		});

		return [
			{
				elem: 'label',
				content: 'Корзина'
			},
			{
				elem: 'table',
				cartItems: cartItems
			}
		];
	}),

	elem('table')(

		tag()('table'),

		content()(function() {

			return [
				{
					elem: 'table-head',
					content: {
						elem: 'table-row',
						content: [
							{
								elem: 'table-hcol',
								content: 'Наименование'
							},
							{
								elem: 'table-hcol',
								content: 'Цена'
							},
							{
								elem: 'table-hcol',
								content: 'Количество'
							},
							{
								elem: 'table-hcol',
								content: 'Стоимость'
							}
						]
					}
				},
				{
					elem: 'table-body',
					content: this.ctx.cartItems.map(function(item) {
						return {
							elem: 'table-row',
							js: { id: item._id },
							content: [
								{
									elem: 'table-col',
									content: [
										{
											elem: 'offer-image',
											attrs: {
												width: '100px',
												height: 'auto',
												src: '/_image?offer=' + item._id + '&index=0'
											}
										},
										{
											elem: 'offer-label',
											content: item.vendor + ' ' + item.model
										}
									]
								},
								{
									elem: 'table-col',
									content: item.price + ' руб'
								},
								{
									elem: 'table-col',
									content: [
										{
											elem: 'top-count',
											content: [
												{
													block: 'fa',
													mix: {
														block: 'cart-table',
														elem: 'count-minus',
														elemMods: { disabled: item.count < 2 ? true : false }
													},
													mods: { icon: 'minus' }
												},
												{
													elem: 'count',
													content: item.count
												},
												{
													block: 'fa',
													mix: {
														block: 'cart-table',
														elem: 'count-plus'
													},
													mods: { icon: 'plus' }
												}
											]
										},
										{
											elem: 'bottom-count',
											content: {
												block: 'link',
												mods: { theme: 'islands', size: 's', view: 'minor', pseudo: true },
												content: 'удалить'
											}
										}
									]
								},
								{
									elem: 'table-col',
									content: {
										elem: 'sum-price',
										content: (Number(item.price) * Number(item.count))  + ' руб'
									}
								}
							]
						}
					})
				}
			];
		})

	),

	elem('table-head').tag()('thead'),
	elem('table-body').tag()('tbody'),
	elem('table-row').tag()('tr'),
	elem('table-hcol').tag()('th'),
	elem('table-col').tag()('td'),

	elem('offer-image').tag()('img')

);
