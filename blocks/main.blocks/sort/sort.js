modules.define('sort', ['i-bem-dom', 'select'], function(provide, bemDom, Select) {

provide(bemDom.declBlock(this.name, {

  	onSetMod: {
        js: {
        	inited: function() {
              this._sortSelect = this.findChildBlock({
                block: Select,
                modName: 'type',
                modVal: 'sort'
              });

              this._showedSelect = this.findChildBlock({
                block: Select,
                modName: 'type',
                modVal: 'showed'
              });

              this._events(this._sortSelect).on('change', this._repaint, this);

              this._events(this._showedSelect).on('change', this._repaint, this);
          }
        }
    },

    getVal: function() {
        return{
            sort: this._sortSelect.getVal(),
            count: this._showedSelect.getVal()
        };
    },

    _repaint: function() {
      this._emit('change', {
          sort: this._sortSelect.getVal(),
          count: this._showedSelect.getVal()
      })
    }

}));

});