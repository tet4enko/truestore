block('sort')(

	content()(function() {
		return [
			{
				elem: 'sort-label',
				mix: { elem: 'label' },
				content: 'Сортировать'
			},
			{
				elem: 'sort-control',
				mix: { elem: 'control' },
				content: {
				    block : 'select',
				    mods : { mode : 'radio', theme : 'islands', size : 'l', type: 'sort' },
				    name : 'select-sort',
				    val : 1,
				    options : [
				        { val : 1, text : 'по умолчанию' },
				        { val : 2, text : 'по убыванию цены' },
				        { val : 3, text : 'по возрастанию цены' },
				        { val : 4, text : 'по наименованию' },
				        { val : 5, text : 'по цвету' }
				    ]
				}
			},
			{
				elem: 'cut-label',
				mix: { elem: 'label' },
				content: 'Показано'
			},
			{
				elem: 'cut-control',
				mix: { elem: 'control' },
				content: {
				    block : 'select',
				    mods : { mode : 'radio', theme : 'islands', size : 'l', type: 'showed' },
				    name : 'select-cut',
				    val : this.ctx.count,
				    options : [
				        { val : 20, text : '20' },
				        { val : 40, text : '40' },
				        { val : 60, text : '60' }
				    ]
				}
			}
		];
	})

);
