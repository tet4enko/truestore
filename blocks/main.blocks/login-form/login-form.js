modules.define('login-form', ['i-bem-dom', 'button', 'input', 'jquery'], function(provide, bemDom, Button, Input, $) {

provide(bemDom.declBlock(this.name, {

	onSetMod: {
        js: {
        	inited: function() {
	            this._events(this.findChildBlock({
	            	block: Button
	            })).on('click', this._onSendClick, this);

	            this._loginInput = this.findChildBlock({
	            	block: Input,
	            	modName: 'type',
	            	modVal: 'login'
	            });

                this._passwordInput = this.findChildBlock({
                    block: Input,
                    modName: 'type',
                    modVal: 'password'
                });
	        }
        }
    },

    _onSendClick: function() {
        $.ajax({
            method: 'POST',
            url: '/_login',
            data: {
                username: this._loginInput.getVal(),
                password: this._passwordInput.getVal()
            },
            success: this._onLoginSuccess.bind(this),
            error: this._onLoginError.bind(this)
        });
    },

    _onLoginSuccess: function(result) {
        if (result && result.status === 'success') {
            location.href = '/admin/offers';
        } else {
            this._onLoginError(result.error);
        }
    },

    _onLoginError: function(err) {
        console.error(err);
        alert('Неудачно:( ' + err);
    }

}));

});