block('login-form')(

	content()(function() {
		return [
			{
				elem: 'field',
				content: {
				    block : 'input',
				    mods : {
				    	theme : 'islands',
				    	size : 'm',
				    	focused : true,
				    	'has-clear': 'yes',
				    	type: 'login'
				    },
				    placeholder : 'Введите логин'
				}
			},
			{
				elem: 'field',
				content: {
				    block : 'input',
				    mods : {
				    	theme : 'islands',
				    	size : 'm',
				    	'has-clear': 'yes',
				    	type: 'password'
				    },
				    placeholder : 'Введите пароль'
				}
			},
			{
				elem: 'buttons',
				content: [
					{
						block: 'button',
						mods: { size: 'm', theme: 'islands', view: 'action', type: 'send' },
						text: 'Войти'
					}
				]
			}
		];
	})

);
