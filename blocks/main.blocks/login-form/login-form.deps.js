({
	mustDeps: ['i-bem-dom'],
    shouldDeps: [
        {
		    block : 'input',
		    mods : {
		    	theme : 'islands',
		    	size : 'm',
		    	focused : true,
		    	'has-clear': 'yes',
		    	type: 'password'
		    }
		},
		{
			block: 'button',
			mods: { size: 'm', theme: 'islands', view: 'action' }
		}
    ]
});
