block('top-popular')(

	elem('img')(

		tag()('img'),

		attrs()(function() {
			return {
				width: '240px',
				height: 'auto',
				src: '/_image?offer=' + this.ctx.data.offer + '&index=0'
			};
		})
	),

	elem('offer-old-price')(
		tag()('del')
	),

	elem('offer-new-price')(
		tag()('span')
	)

);
