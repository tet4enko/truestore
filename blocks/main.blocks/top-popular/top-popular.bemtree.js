block('top-popular')(

	elem('img')(

		tag()('img'),

		attrs()(function() {
			return {
				width: '240px',
				height: 'auto',
				src: '/_image?offer=' + this.ctx.data.offer + '&index=0'
			};
		})
	),

	content()(function() {
		var popular = this.data.popular[0];

		return popular ? [
			{
				elem: 'label',
				content: 'Популярное'
			},
			{
				elem: 'content',
				content: {
					block: 'link',
					mods: { theme: 'islands', size: 'm' },
					url: '/catalog/' + popular.tag,
					content: [
						{
							block: 'top-popular',
							elem: 'img',
							data: {
								offer: popular._id
							}
						},
						{
							block: 'top-popular',
							elem: 'offer-price',
							content: [
								{
									block: 'top-popular',
									elem: 'offer-new-price',
									content: popular.price + ' руб'
								},
								popular.oldPrice && {
									block: 'top-popular',
									elem: 'offer-old-price',
									content: popular.oldPrice + ' руб'
								}
							]
						},
						{
							block: 'top-popular',
							elem: 'offer-label',
							content: popular.vendor + ' ' + popular.model
						},
						{
							block: 'top-popular',
							elem: 'offer-description',
							content: popular.description
						}
					]
				}
			}
		] : '';
	})

);
