modules.define('i-localstorage', ['i-bem-dom'], function(provide, bemDom) {

provide(bemDom.declBlock(this.name, {}, function() {


    var ls = null;

    try {
        ls = window['localStorage'];
    } catch(e) {}


    /**
     * SafeLS - safety localStorage usage!
     * Little JavaScript object that wrap every localStorage method in try-catch because every method throws exceptions, not only setItem.
     * You can safely use this wrapper even browser has no localStorage support.
     * @name SafeLS
     */
    return {
        /**
         * Store key.
         * @param {String} key
         * @param {String} data
         * @return {Boolean}
         */
        setItem: function(key, data) {
            try {
                ls.setItem(key, data);
                return true;
            } catch (e) {
                // iPad workaround
                if (this.removeItem(key)) {
                    try {
                        ls.setItem(key, data);
                        return true;
                    } catch (e) { this._errorHandler(e) };
                }
                this._errorHandler(e);
            }
            return false;
        },

        /**
         * Get key.
         * @param {String} key
         * @return {String}
         */
        getItem: function(key) {
            try {
                return ls.getItem(key);
            } catch (e) { this._errorHandler(e) };

            return null;
        },

        /**
         * Clear storage.
         * @return {Boolean}
         */
        clear: function() {
            try {
                ls.clear();
                return true;
            } catch (e) { this._errorHandler(e) };

            return false;
        },

        /**
         * Remove key.
         * @param {String} key
         * @return {Boolean}
         */
        removeItem: function(key) {
            try {
                ls.removeItem(key);
                return true;
            } catch (e) { this._errorHandler(e) };

            return false;
        },

        /**
         * Return storage length.
         * @return {Number}
         */
        length: function() {
            try {
                return ls.length;
            } catch (e) { this._errorHandler(e) };

            return 0;
        },

        /**
         * Return key name by index.
         * @param {Number} index
         * @return {String}
         */
        key: function(index) {
            try {
                return ls.key(index);
            } catch (e) { this._errorHandler(e) };

            return '';
        },

        _errorHandler: function(e) { }
    };


}()));

});