block('similar')(

	content()(function() {
		var items = this.data.similar;

		return items.length ? [
			{
				elem: 'label',
				content: 'Похожие товары'
			},
			{
				elem: 'items',
				content: items.map(function(item) {
					return {
						elem: 'item',
						data: item
					};
				})
			}
		] : '';
	})

);
