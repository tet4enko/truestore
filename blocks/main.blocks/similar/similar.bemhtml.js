block('similar')(

	elem('item').content()(function() {
		var data = this.ctx.data;

		return {
			block: 'link',
			mods: { size: 's', theme: 'islands' },
			url: '/catalog/' + data.tag,
			content: [
				{
					block: 'similar',
					elem: 'img-wrapper',
					content: {
						block: 'similar',
						elem: 'img',
						data: {
							offer: data._id,
							name: data.name
						}
					} 
				},
				{
					block: 'similar',
					elem: 'name',
					content: data.name
				},
				{
					block: 'similar',
					elem: 'color',
					attrs: { style: 'background: ' + data.colorHex + ';' }
				},
				{
					block: 'similar',
					elem: 'price',
					content: data.price + ' руб'
				}
			]
		};
	}),

	elem('img')(

		tag()('img'),

		attrs()(function() {
			return {
				width: '150px',
				height: 'auto',
				src: '/_image?offer=' + this.ctx.data.offer + '&index=0',
				alt: 'Похожее: ' + this.ctx.data.name
			};
		})
	)

);
