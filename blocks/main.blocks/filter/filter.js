modules.define('filter', ['i-bem-dom', 'checkbox-group'], function(provide, bemDom, CheckboxGroup) {

provide(bemDom.declBlock(this.name, {

	onSetMod: {
        js: {
        	inited: function() {
        		this._checkboxGroup = this.findChildBlock(CheckboxGroup);

	            this._events(this._checkboxGroup).on('change', this._onChange, this);
	        }
        }
    },

    getVal: function() {
        var checkboxGroupVal = this._checkboxGroup.getVal();
        var val;

        if (!checkboxGroupVal.length) {
            val = this.params.options;
        } else {
            val = checkboxGroupVal;
        }

        return {
            field: this.params.field,
            data: val
        }; 
    },

    _onChange: function(e) {
        this._emit('change', this.getVal());
    },

}));

});