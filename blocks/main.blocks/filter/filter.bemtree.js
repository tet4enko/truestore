block('filter')(

	wrap()(function() {
		return {
			tag: 'noindex',
			content: this.ctx
		}
	}),

	content()(function() {

		return [
			{
				elem: 'label',
				content: this.ctx.label
			},
			{
			    block : 'checkbox-group',
			    mods : { theme : 'islands', size : 'l' },
			    name : this.ctx.field + '#checkbox-group',
			    val: [],
			    options : this.ctx.options
			}
		];
	})

);
