({
    shouldDeps: [
        { block: 'textarea', mods: { theme: 'islands', size: 'm' } },
        { block: 'input', mods: { theme: 'islands', size: 'm', 'has-clear': true, focused: true } },
        { block: 'button', mods: { theme: 'islands', size: 'm', view: 'action' } },
        { block : 'modal', mods : { theme : 'islands', autoclosable : true } }
    ]
});
