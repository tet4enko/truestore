block('store-content').mod('view', 'delivery')(

	content()(function() {
		return [
			{
				elem: 'label',
				content: 'Купить в нашем магазине'
			},
			{
				elem: 'main-content',
				content: [
					{
						elem: 'geo',
						content: 'Магазин TRUE-STORE находится в Симферополе. Доставка осуществляется по Крыму.'
					},
					{
						elem: 'howto1',
						content: 'Чтобы заказать товар, Вам достаточно выбрать понравившуюся модель и нажать кнопку «Купить».'
					},
					{
						elem: 'howto2',
						content: [
							{
								block: 'link',
								mods: { theme: 'islands', size: 'm', view: 'minor' },
								url: '/cart',
								content: 'Перейдите к оформлению заказа'
							},
							", выберите подходящий вариант получения товара. Оставьте в форме заказа свой контактный телефон, и мы в ближайшее время перезвоним Вам для подтверждения заказа и уточнения делатей."
						]
					}
				]
			},
			{
				elem: 'label',
				content: 'Возможные варианты получения товара:'
			},
			{
				elem: 'items',
				tag: 'ol',
				content: [
					{
						elem: 'item',
						tag: 'li',
						content: [
							{
								elem: 'pickup-line1',
								content: 'Самовывоз из пункта выдачи. Пункт выдачи работает со вторника по воскресенье с 10.00 до 16.00 (Оплата при получении товара)'
							},
							{
								elem: 'pickup-line2',
								content: 'Адрес: Республика Крым, Симферополь, Объездная улица, 10 (Радиорынок). Бутик № а25/a26'
							},
							{
								elem: 'pickup-address-map',
								tag: 'iframe',
								attrs: {
									src: 'https://api-maps.yandex.ru/frame/v1/-/CZdIF6-h',
									width: '600',
									height: '300',
									frameborder: '0'
								}
							}
						]
					},
					{
						elem: 'item',
						tag: 'li',
						content: 'Доставка курьером по Симферополю в день заказа при оформлении до 18:00 (ежедневно, бесплатно, оплата при получении товара)'
					},
					{
						elem: 'item',
						tag: 'li',
						content: [
							{
								elem: 'delivery-line1',
								content: 'Доставка с помощью "Быстрая Почта" по Крыму в течении трех рабочих дней – 400 руб.'
							},
							{
								elem: 'delivery-line2',
								content: 'Оплата при получении товара (Товар отправляется с наложенным платежом)'
							}
						]
					}
				]
			}
		];
	})

);
