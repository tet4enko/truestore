block('store-content').mod('view', 'offer')(

	content()(function() {
		return [
			{
				block: 'top-popular'
			},
			{
				elem: 'main',
				content: [
					{
						elem: 'links',
						content: [
							{
								block: 'link',
								mods: { theme: 'islands', size: 'm', view: 'minor' },
								url: '/',
								content: 'Главная'
							},
							' > ',
							this.data.offerData.name
						]
					},
					{
						block: 'offer',
						mods: { view: 'full' },
						data: this.data.offerData
					},
					{
						block: 'similar'
					},
					{
					    block : 'modal',
					    mods : { theme : 'islands', autoclosable : true, type: 'propose-cart' },
					    mix: { block: 'catalog', elem: 'propose-cart-modal' },
					    content : [
					    	{
					    		elem: 'propose-cart-label',
					    		content: 'Товар добавлен в корзину!'
					    	},
					    	{
					    		elem: 'propose-cart-buttons',
					    		content: [
					    			{
					    				block: 'button',
					    				mods: { theme: 'islands', size: 'm', type: 'goback' },
					    				text: 'Вернуться к покупкам'
					    			},
					    			{
					    				block: 'button',
					    				mods: { theme: 'islands', size: 'm', view: 'action', type: 'gocart' },
					    				text: 'Перейти к оформлению'
					    			}
					    		]
					    	}
					    ]
					}
				]
			}
		];
	})

);
