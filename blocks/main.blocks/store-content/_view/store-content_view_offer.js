modules.define('store-content', ['i-bem-dom', 'events__channels', 'jquery', 'offer', 'button', 'input', 'modal', 'BEMHTML'],
	function(provide, bemDom, channels, $, Offer, Button, Input, Modal, BEMHTML, StoreContent) {

	provide(StoreContent.declMod({ modName : 'view', modVal : 'offer' }, {

		onSetMod: {
	        js: {
	        	inited: function() {
	        		this._proposeCartModal = this.findChildBlock({
		            	block: Modal,
		            	modName: 'type',
		            	modVal: 'propose-cart'
		            });

		            this._domEvents(this._proposeCartModal.findChildBlock({
		                block: Button,
		                modName: 'type',
		                modVal: 'goback'
		              }).domElem)
		                    .on('pointerclick', this._onGoBackButtonClick, this);

		              this._domEvents(this._proposeCartModal.findChildBlock({
		                block: Button,
		                modName: 'type',
		                modVal: 'gocart'
		              }).domElem)
		                    .on('pointerclick', this._onGoCartButtonClick, this);

	        		this._events(Offer).on('add', this._onOfferAdd, this);
		        }
	        }
	    },

	    _onOfferAdd: function(e, data) {
	    	this._proposeCartModal.setMod('visible', true);

    		channels('cart').emit('change', data);
	    },

	    _onGoBackButtonClick: function() {
	    	location.href = '/';
	    },

	    _onGoCartButtonClick: function() {
	    	location.href = '/cart';
	    }

	}));

});
