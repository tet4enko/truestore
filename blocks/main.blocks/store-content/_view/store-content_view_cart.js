modules.define('store-content', ['i-bem-dom', 'events__channels', 'jquery', 'button', 'input', 'textarea', 'modal', 'cart-table', 'delivery-options', 'i-localstorage', 'BEMHTML'],
	function(provide, bemDom, channels, $, Button, Input, Textarea, Modal, CartTable, DeliveryOptions, iLocalstorage, BEMHTML, StoreContent) {

	provide(StoreContent.declMod({ modName : 'view', modVal : 'cart' }, {

		onSetMod: {
	        js: {
	        	inited: function() {
	        		this._cartTable = this.findChildBlock(CartTable);
	        		this._deliveryOptions = this.findChildBlock(DeliveryOptions);

	        		this._phoneInput = this.findChildBlock({
	        			block: Input,
	        			modName: 'type',
	        			modVal: 'phone'
	        		});
	        		this._commentInput = this.findChildBlock({
	        			block: Textarea,
	        			modName: 'type',
	        			modVal: 'comment'
	        		});

	        		this._submitButton = this.findChildBlock({
	        			block: Button,
	        			modName: 'type',
	        			modVal: 'submit'
	        		});

	        		this._successSubmitModal = this.findChildBlock(Modal);

	        		this._domEvents(this._successSubmitModal.findChildBlock(Button).domElem)
                    	.on('pointerclick', this._onModalOkClick, this);

	        		this._events(this._successSubmitModal).on({ modName: 'visible', modVal: '' }, this._onModalClose, this);

	        		this._render();
		        }
	        }
	    },

	    _render: function() {
	    	var cartData = iLocalstorage.getItem('cart');

	    	cartData = cartData ? JSON.parse(cartData) : {};

	    	if (!Object.keys(cartData).length) {
	    		bemDom.update(this.domElem, BEMHTML.apply({
	    			block: 'store-content',
	    			elem: 'empty'
	    		}));
	    	} else {
		    	this._totalSum = this._getTotalSum(cartData);

		        this._cartTable.render(cartData);
		        bemDom.update(this.findChildElem('total-value').domElem, this._totalSum + ' руб');

		        this._events(this._deliveryOptions).on('change', this._onDeliveryChange, this);
		        channels('cart').on('change', this._onCartChange, this);

		        this._domEvents(this._submitButton.domElem)
                    .on('pointerclick', this._onSubmitButtonClick, this);
		    }

	        this.setMod('visible', true);
	    },

	    _onDeliveryChange: function(e, data) {
	    	bemDom.update(this.findChildElem('total-value').domElem, (this._totalSum + this.__self._sums[data]) + ' руб');
	    },

	    _onCartChange: function() {
	    	var cartData = iLocalstorage.getItem('cart');

	    	cartData = cartData ? JSON.parse(cartData) : {};

	    	if (!Object.keys(cartData).length) {
	            bemDom.update(this.domElem, BEMHTML.apply({
	                block: 'store-content',
	                elem: 'empty'
	            }));
	        } else {
	        	this._totalSum = this._getTotalSum(cartData);

	    		bemDom.update(this.findChildElem('total-value').domElem, (this._totalSum + this.__self._sums[this._deliveryOptions.getVal()]) + ' руб');
	        }
	    },

	    _getTotalSum: function(cartData) {
	    	var totalSum = 0;

	    	Object.keys(cartData).forEach(function(offerId) {
				var offerData = this.params.offers.filter(function(offer) {
					return offer._id.toString() === offerId.toString();
				})[0];

				if (offerData) {
					totalSum += Number(offerData.price) * Number(cartData[offerId]);
				}
			}, this);

			return totalSum;
	    },

	    _onSubmitButtonClick: function() {
	    	var phone = this._phoneInput.getVal();
	    	var comment = this._commentInput.getVal();
	    	var recaptchaVal = grecaptcha.getResponse();

	    	if (!phone) {
	    		alert('Введите, пожалуйста, телефон.');
	    		this._phoneInput.setMod('focused', true);
	    	} else if (!recaptchaVal) {
	    		alert('Подтвердите, что Вы не робот!');
	    	} else {
	    		var cartData = iLocalstorage.getItem('cart');
				cartData = cartData ? JSON.parse(cartData) : {};
				var orderItems = [];
				var sum = 0;

				Object.keys(cartData).forEach(function(offerId) {
					var offerData = this.params.offers.filter(function(offer) {
						return offer._id.toString() === offerId.toString();
					})[0];

					if (offerData) {
						orderItems.push({
							offerId: offerData._id,
							offerCount: Number(cartData[offerId]),
							offerPrice: Number(offerData.price),
							offerName: offerData.vendor + ' ' + offerData.model,
							offerColor: offerData.color
						});

						sum += Number(offerData.price) * Number(cartData[offerId]);
					}
				}, this);

				$.ajax({
		    		method: 'POST',
		    		url: '/cart/saveorder/',
	 	    		data: {
	 	    			items: orderItems,
	 	    			totalSum: sum,
	 	    			deliveryType: this._deliveryOptions.getVal(),
	 	    			phone: phone,
	 	    			comment: comment,
	 	    			recaptchaVal: recaptchaVal
	 	    		},
		    		success: this._onSaveSuccess.bind(this),
		    		error: this._onSaveError.bind(this)
		    	});
	    	}
	    },

	    _onSaveSuccess: function(data) {
	    	if (data.status === 'success') {
		    	this._successSubmitModal.setMod('visible', true);

		    	iLocalstorage.clear();
	    	} else {
	    		this._onSaveError("status !== 'success'")
	    	}
	    },

	    _onSaveError: function() {
	    	alert('Не удалось сохранить заказ :( Попробуйте, пожалуйста, позже.')
	    },

	    _onModalOkClick: function() {
	    	this._successSubmitModal.delMod('visible');
	    },

	    _onModalClose: function() {
	    	location.href =  '/';
	    }

	}));

});
