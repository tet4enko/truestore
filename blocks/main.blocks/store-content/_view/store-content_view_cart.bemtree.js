block('store-content').mod('view', 'cart')(

	content()(function() {
		return [
			{
				block: 'cart-table',
				offers: this.data.offers
			},
			{
				block: 'delivery-options'
			},
			{
				elem: 'total',
				content: [
					{
						elem: 'total-label',
						content: 'Итого:'
					},
					{
						elem: 'total-value'
					}
				]
			},
			{
				elem: 'submit',
				content: [
					{
						elem: 'submit-phone',
						content: {
							block: 'input',
							mods: { theme: 'islands', size: 'm', 'has-clear': true , type: 'phone' },
							placeholder: 'Телефон'
						}
					},
					{
						elem: 'submit-comment',
						content: {
						    block : 'textarea',
						    mods : { theme : 'islands', size: 'm' , type: 'comment' },
						    placeholder : 'Комментарий к заказу (Адрес доставки, при необходмости)'
						}
					},
					{
						elem: 'recaptcha',
						cls: 'g-recaptcha',
						bem: false,
						attrs: {
							'data-sitekey': this.env.recaptcha.public
						}
					},
					{
						block: 'button',
						mods: { theme: 'islands', size: 'm', view: 'action', type: 'submit' },
						text: 'Офомить заказ'
					}
				]
			},
			{
			    block : 'modal',
			    mods : { theme : 'islands', autoclosable : true, type: 'success-submit-order' },
			    content : [
			    	{
			    		elem: 'order-saved',
			    		content: 'Ваш заказ принят!'
			    	},
			    	{
			    		elem: 'approve',
			    		content: 'В ближайшее время мы свяжемся с Вами для подтверждения.'
			    	},
			    	{
			    		elem: 'buttons',
			    		content: {
			    			block: 'button',
			    			mods: { theme: 'islands', size: 'm' },
			    			text: 'Хорошо'
			    		}
			    	}
			    ]
			}
		];
	})

);
