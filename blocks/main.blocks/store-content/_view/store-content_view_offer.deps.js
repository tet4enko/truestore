({
    shouldDeps: [
        { block: 'top-popular' },
        { block: 'offer', mods: { view: 'full' } },
        { block: 'link', mods: { theme: 'islands', size: 'm', view: 'minor' } },
        { block: 'similar' },
        { block : 'modal', mods : { theme : 'islands', autoclosable : true } },
    ]
});
