block('store-content').mod('view', 'catalog')(

	content()(function() {
		return [
			{
				block: 'filter-bar'
			},
			{
				block: 'catalog',
				offers: this.data.offers,
				count: 20
			}
		];
	})

);
