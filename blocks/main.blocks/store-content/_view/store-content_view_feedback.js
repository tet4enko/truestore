modules.define('store-content', ['i-bem-dom', 'jquery', 'button', 'input', 'textarea', 'modal', 'BEMHTML'],
	function(provide, bemDom, $, Button, Input, Textarea, Modal, BEMHTML, StoreContent) {

	provide(StoreContent.declMod({ modName : 'view', modVal : 'feedback' }, {

		onSetMod: {
	        js: {
	        	inited: function() {
	        		this._senderInput = this.findChildBlock({
	        			block: Input,
	        			modName: 'type',
	        			modVal: 'sender'
	        		});
	        		this._commentInput = this.findChildBlock({
	        			block: Textarea,
	        			modName: 'type',
	        			modVal: 'comment'
	        		});

	        		this._submitButton = this.findChildBlock({
	        			block: Button,
	        			modName: 'type',
	        			modVal: 'submit'
	        		});

	        		this._successSubmitModal = this.findChildBlock(Modal);

	        		this._events(this._successSubmitModal).on({ modName: 'visible', modVal: '' }, this._onSuccessSubmitPopupClose, this);
	        		this._domEvents(this._submitButton.domElem)
                    	.on('pointerclick', this._onSubmitButtonClick, this);
	        		this._events(this._successSubmitModal.findChildBlock(Button)).on('click', this._onSuccessSubmitPopupOkClick, this);
		        }
	        }
	    },

	    _onSubmitButtonClick: function() {
	    	var sender = this._senderInput.getVal();
	    	var comment = this._commentInput.getVal();
	    	var recaptchaVal = grecaptcha.getResponse();

	    	if (!sender) {
	    		alert('Введите, пожалуйста, телефон или e-mail!');
	    	} else if (!recaptchaVal) {
	    		alert('Подтвердите, что вы не робот!');
	    	} else {
	    		$.ajax({
		    		method: 'POST',
		    		url: '/feedback',
	 	    		data: {
	 	    			sender: sender,
	 	    			comment: comment,
	 	    			recaptchaVal: recaptchaVal
	 	    		},
		    		success: this._onSubmitSuccess.bind(this),
		    		error: this._onSubmitError.bind(this)
		    	});
	    	}
	    },

	    _onSubmitSuccess: function(data) {
	    	if (data.status === 'success') {
	    		this._successSubmitModal.setMod('visible', true);
	    	} else {
	    		this._onSubmitError('status !== success');
	    	}
	    },

	    _onSubmitError: function(err) {
			console.error(err);
	    	alert('Не удалось отправить запрос:( Попробуйте позже.');
	    },

	    _onSuccessSubmitPopupClose: function() {
	    	location.href = '/';
	    },

	    _onSuccessSubmitPopupOkClick: function() {
	    	this._successSubmitModal.delMod('visible');
	    }
	}));

});
