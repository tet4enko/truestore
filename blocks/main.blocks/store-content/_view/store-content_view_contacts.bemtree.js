block('store-content').mod('view', 'contacts')(

	content()(function() {
		return [
			{
				elem: 'label',
				content: 'Наши контакты:'
			},
			{
				elem: 'contacts-list',
				tag: 'ul',
				content: [
					{
						elem: 'contact',
						tag: 'li',
						content: [
							'Телефон: ',
							{
								block: 'link',
								mods: { theme: 'islands', size: 'm', view: 'minor' },
								content: '+79780241117',
								url: 'tel:+79780241117'
							},
							' – для консультаций ежедневно с 8:00 до 20:00 без выходных!'
						]
					},
					{
						elem: 'contact',
						tag: 'li',
						content: [
							'Email: ',
							{
								block: 'link',
								mods: { theme: 'islands', size: 'm', view: 'minor' },
								content: 'thisistruestore@gmail.com',
								url: 'mailto:thisistruestore@gmail.com'
							}
						]
					}
				]
			},
			{
				elem: 'label',
				content: 'Пункт самовывоза'
			},
			{
				elem: 'pickup-address',
				content: 'Адрес: Республика Крым, Симферополь, Объездная улица, 10 (Радиорынок). Бутик № а25/a26'
			},
			{
				elem: 'pickup-address-map',
				tag: 'iframe',
				attrs: {
					src: 'https://api-maps.yandex.ru/frame/v1/-/CZdIF6-h',
					width: '600',
					height: '300',
					frameborder: '0'
				}
			},
			{
				elem: 'label',
				content: 'Юридическая информация'
			},
			{
				elem: 'pickup-address',
				content: 'ИП «МАРЧЕНКОВ ВЛАДИМИР АНАТОЛЬЕВИЧ». ОГРНИП: 315910200347745.'
			}
		];
	})

);
