({
    shouldDeps: [
        { block: 'cart-table' },
        { block: 'delivery-options' },
        { block: 'textarea', mods: { theme: 'islands', size: 'm' } },
        { block: 'input', mods: { theme: 'islands', size: 'm', 'has-clear': true, focused: true } },
        { block: 'button', mods: { theme: 'islands', size: 'm', view: 'action' } },
        { block: 'link', mods: { theme: 'islands', size: 'm', view: 'minor' } },
        { block : 'modal', mods : { theme : 'islands', autoclosable : true, visible: true } }
    ]
});
