block('store-content').mod('view', 'feedback')(

	content()(function() {
		return [
			{
				elem: 'label',
				content: 'Здесь Вы можете задать свой вопрос или оставить отзыв о нашем магазине.'
			},
			{
				elem: 'field',
				content: [
					{
						elem: 'field-label',
						content: 'Телефон или e-mail:'
					},
					{
						elem: 'field-control',
						content: {
							block: 'input',
							mods: { theme: 'islands', size: 'm', 'has-clear': true, focused: true, type: 'sender' }
						}
					}
				]
			},
			{
				elem: 'field',
				content: [
					{
						elem: 'field-label',
						content: 'Вопрос или комментарий:'
					},
					{
						elem: 'field-control',
						content: {
							block: 'textarea',
							mods: { theme: 'islands', size: 'm', type: 'comment' }
						}
					}
				]
			},
			{
				elem: 'recaptcha',
				cls: 'g-recaptcha',
				bem: false,
				attrs: {
					'data-sitekey': this.env.recaptcha.public
				}
			},
			{
				elem: 'buttons',
				content: {
					block: 'button',
					mods: { theme: 'islands', size: 'm', view: 'action', type: 'submit' },
					text: 'Отправить'
				}
			},
			{
			    block : 'modal',
			    mix: { elem: 'success-submit-modal' },
			    mods : { theme : 'islands', autoclosable : true, type: 'success-submit-feedback' },
			    content : [
			    	{
			    		elem: 'sended',
			    		content: 'Ваш комментарий отправлен!'
			    	},
			    	{
			    		elem: 'response',
			    		content: 'В ближайшее время мы Вам ответим.'
			    	},
			    	{
			    		elem: 'buttons',
			    		content: {
			    			block: 'button',
			    			mods: { theme: 'islands', size: 'm' },
			    			text: 'Хорошо'
			    		}
			    	}
			    ]
			}
		];
	})

);
