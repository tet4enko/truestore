block('store-content')(

	elem('empty').content()([
		{
			elem: 'empty-topline',
			content: 'Ваша корзина пуста.'
		},
		{
			elem: 'empty-bottomline',
			content: [
				'Перейдите в ',
				{
					block: 'link',
					mods: { theme: 'islands', size: 'm', view: 'minor' },
					url: '/catalog',
					content: 'каталог'
				},
				' для добавления товара в корзину.'
			]
		}
	])

);
