modules.define('store-content', ['i-bem-dom'],
	function(provide, bemDom) {

provide(bemDom.declBlock(this.name, {}, {

    _sums: {
        pickup: 0,
        courier: 0,
        delivery: 400
    }
    
}));

});
