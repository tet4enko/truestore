modules.define('delivery-options', ['i-bem-dom', 'radio-group', 'BEMHTML'],
	function(provide, bemDom, RadioGroup, BEMHTML) {

provide(bemDom.declBlock(this.name, {

  	onSetMod: {
        js: {
          	inited: function() {
                this._radioGroup = this.findChildBlock(RadioGroup);

                this._events(this._radioGroup).on('change', this._onChange, this);
            }
        }
    },

    getVal: function() {
        return this._radioGroup.getVal();
    },

    _onChange: function(e, data) {
        var value = e.bemTarget.getVal();

        bemDom.update(this.findChildElem('description').domElem, BEMHTML.apply({
            block: 'delivery-options',
            elem: value + '-desc'
        }));

        this._emit('change', value);
    }

}));

});
