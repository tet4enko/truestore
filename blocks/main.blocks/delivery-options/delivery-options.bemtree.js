block('delivery-options')(

	js()(function() {
		return { offers: this.ctx.offers }
	}),

	content()(function() {
		return [
			{
				elem: 'label',
				content: 'Выберите вариант получения товара:'
			},
			{
			    block : 'radio-group',
			    mods : { theme : 'islands', size : 'l' },
			    name : 'radio-button',
			    val : 'pickup',
			    options : [
			        { val : 'pickup', text : 'Самовывоз из пункта выдачи. ВТ-ВС 10.00-16.00' },
			        { val : 'courier', text : 'Доставка курьером по Симферополю в день заказа при оформлении до 18:00' },
			        { val : 'delivery', text : 'Доставка с помощью "Быстрая Почта" по Крыму в течении трех рабочих дней – 400 руб' }
			    ]
			},
			{
				elem: 'description',
				content: {
					elem: 'pickup-desc'
				}
			}
		];
	})

);
