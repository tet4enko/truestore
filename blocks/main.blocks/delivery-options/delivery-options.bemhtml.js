block('delivery-options')(

	js()(true),

	elem('pickup-desc').content()(function() {
		return [
			{
				elem: 'pickup-address-label',
				content: 'Адрес пункта самовывоза:'
			},
			{
				elem: 'pickup-address',
				content: 'Республика Крым, Симферополь, Объездная улица, 10 (Радиорынок). Бутик № а25/a26.'
			},
			{
				elem: 'pickup-address-map',
				tag: 'iframe',
				attrs: {
					src: 'https://api-maps.yandex.ru/frame/v1/-/CZdIF6-h',
					width: '600',
					height: '200',
					frameborder: '0'
				}
			}
		];
	}),

	elem('courier-desc').content()(function() {
		return [
			{
				elem: 'courier-desc-label',
				content: 'Введите, пожалуйста, Ваш адрес в комментарии к заказу.'
			}
		];
	}),

	elem('delivery-desc').content()(function() {
		return [
			{
				elem: 'delivery-desc-label',
				content: 'Введите, пожалуйста, Ваш адрес в комментарии к заказу.'
			},
			{
				elem: 'delivery-desc-label2',
				content: 'После подтверждения заказа, он будет отправлен по указанному адресу с наложенным платежом.'
			},
			{
				elem: 'delivery-desc-label2',
				content: 'Вам в SMS будет отправлен номер посылки в "Быстрой почте", по которой Вы сможете отследить её статус.'
			}
		];
	})

);
