({
	mustDeps: [
		'i-bem-dom',
		{
            block: 'i-localstorage'
        }
	],
    shouldDeps: [
        { block: 'radio-group', mods: { theme: 'islands', size: 'l' } }
    ]
});