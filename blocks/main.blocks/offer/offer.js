modules.define('offer', ['i-bem-dom', 'button', 'i-localstorage'], function(provide, bemDom, Button, iLocalstorage) {

provide(bemDom.declBlock(this.name, {

	onSetMod: {
        js: {
        	inited: function() {
                var addToCartButton = this.findChildBlock({ block: Button, modName: 'type', modVal: 'add'});

                if (addToCartButton) {
                    this._domEvents(addToCartButton.domElem)
                        .on('pointerclick', this._addToCard, this);
                }

                if (this._isInCart()) {
                    this.setMod('in-cart', true);
                }
	        }
        },
        'in-cart': {
            'true': function() {
                bemDom.update(this
                    .findChildBlock({ block: Button, modName: 'type', modVal: 'add'})
                    .findChildElem('text')
                    .domElem, 'В корзине')
            }
        }
    },

    _addToCard: function() {
        var cartData = iLocalstorage.getItem('cart');

        cartData = cartData ? JSON.parse(cartData) : {};

        if (cartData[this.params.data._id]) {
            cartData[this.params.data._id]++;
        } else {
            cartData[this.params.data._id] = 1;
        }

        iLocalstorage.setItem('cart', JSON.stringify(cartData));

        this.setMod('in-cart', true);

        this._emit('add', { id: this.params.data._id });
    },

    _isInCart: function() {
        var cartData = iLocalstorage.getItem('cart');

        if (!cartData) {
            return false;
        } else {
            cartData = JSON.parse(cartData);

            if (cartData[this.params.data._id]) {
                return true;
            } else {
                return false;
            }
        }
    }

}));

});