block('offer')(

	attrs()({
		itemscope: '',
		itemtype: 'https://schema.org/Product'
	}),

	elem('img')(

		tag()('img'),

		attrs()(function() {
			return {
				width: '240px',
				height: 'auto',
				src: '/_image?offer=' + this.ctx.data.offer + '&index=0',
				alt: this.ctx.data.name,
				itemprop: 'image'
			};
		})
	)

);
