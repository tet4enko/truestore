block('offer').mod('view', 'full')(

	js()(function() {
		return { data: this.ctx.data };
	}),

	content()(function() {
		var data = this.ctx.data;

		return [
			{
				elem: 'left',
				content: {
					elem: 'img',
					data: {
						offer: data._id,
						name: data.name
					}
				}
			},
			{
				elem: 'right',
				content: [
					{
						elem: 'label',
						attrs: {
							itemprop: 'name'
						},
						content: data.vendor + ' ' + data.model
					},
					{
						elem: 'description',
						attrs: {
							itemprop: 'description'
						},
						content: data.description
					},
					{
						elem: 'price',
						attrs: {
							itemprop: 'offers',
							itemscope: '',
							itemtype: 'https://schema.org/Offer'
						},
						content: [
							{
								tag: 'meta',
								attrs: {
									itemprop: 'price',
									content: data.price
								}
							},
							{
								tag: 'meta',
								attrs: {
									itemprop: 'priceCurrency',
									content: 'RUB'
								}
							},
							'Цена: ',
							{
								elem: 'new-price',
								tag: 'span',
								content: data.price + ' руб'
							},
							data.oldprice && {
								elem: 'old-price',
								tag: 'del',
								content: data.oldprice  + ' руб'
							}
						]
					},
					{
						elem: 'controls',
						content: [
							data.available ? {
								block: 'button',
								mods: { theme: 'islands', size: 'l', type: 'add', view: 'action' },
								text: 'КУПИТЬ'
							} : {
								elem: 'not-available',
								content: 'Нет в наличии'
							}
						]
					}
				]
			}
		];
	})

);
