({
	mustDeps: 'i-bem-dom',
    shouldDeps: [
    	{ mods: { 'in-cart': true } },
        { block: 'i-localstorage' },
        { block: 'button', mods: { theme: 'islands', size: 'l', view: 'action' } }
    ]
});
