block('fa')(
	js()(true),
    tag()('i'),
    cls()(function() { return this.ctx.mods.icon && ('fa-' + this.ctx.mods.icon); })
);
