({
	mustDeps: [
		'i-bem-dom',
		{
            block: 'events',
            elem: 'channels'
        }
	],
    shouldDeps: [
        { block: 'filter' }
    ]
});
