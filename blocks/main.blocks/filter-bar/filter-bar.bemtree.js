block('filter-bar')(

	content()(function() {
		return [
			{
				elem: 'label',
				content: 'Подобрать'
			},
			{
				elem: 'filters',
				content: [
					{
						block: 'filter',
						label: 'Производитель',
						field: 'vendor',
						options: Object.keys(this.data.filterData.vendor).map(function(vendor) {
							return {
								text: vendor + ' (' + this.data.filterData.vendor[vendor] + ')',
								val: vendor
							};
						}.bind(this))
					},
					{
						block: 'filter',
						label: 'Цвет',
						field: 'color',
						options: Object.keys(this.data.filterData.color).map(function(color) {
							return {
								text: color + ' (' + this.data.filterData.color[color] + ')',
								val: color
							};
						}.bind(this))
					},
					{
						block: 'filter',
						label: 'Встроенная память',
						field: 'internalMemory',
						options: Object.keys(this.data.filterData.internalMemory).map(function(memory) {
							return {
								text: memory + 'gb (' + this.data.filterData.internalMemory[memory] + ')',
								val: memory
							};
						}.bind(this))
					}
				]
			}
		];
	})

);
