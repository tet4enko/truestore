modules.define('filter-bar', ['events__channels', 'i-bem-dom', 'filter'], function(provide, channels, bemDom, Filter) {

provide(bemDom.declBlock(this.name, {

	onSetMod: {
        js: {
        	inited: function() {
        		this._filters = this.findChildBlocks(Filter);

	            this._events(this._filters).on('change', this._onChange, this);
	        }
        }
    },

    _onChange: function(e) {
        debugger;
        channels('filters').emit('change', this._filters.map(function(filter) {
            return filter.getVal();
        }));
    }

}));

});