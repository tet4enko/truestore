block('catalog')(

	wrap()(function() {
		var ctx = this.ctx;
		var mods = ctx.mods || {};

		mods.more = this.ctx.offers.length > this.ctx.count;

		ctx.mods = mods;
		return ctx;
	}),

	js()(function() {
		return {
			count: this.ctx.count,
			offers: this.ctx.offers
		};
	}),

	elem('offers').content()(function() {
		var count = this.ctx.count;
		var viewed = this.ctx.offers.slice(0, count);

		if (viewed.length) {
			return viewed.map(function(offer) {
				return {
					block: 'offer',
					mods: { view: 'default' },
					data: offer
				};
			});
		} else {
			return {
				elem: 'empty',
				content: 'Извините, список товаров пуст :('
			};
		}
	})

);
