({
	mustDeps: [
		'i-bem-dom',
		{
            block: 'events',
            elem: 'channels'
        }
	],
    shouldDeps: [
        { block: 'sort' } ,
        { block: 'offer', mods: { sale: true, view: 'default'  } },
        { block: 'button', mods: { size: 'l', theme: 'islands' } },
        { block : 'modal', mods : { theme : 'islands', autoclosable : true, visible: true } }
    ]
});
