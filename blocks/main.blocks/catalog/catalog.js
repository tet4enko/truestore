modules.define('catalog', ['i-bem-dom', 'events__channels', 'button', 'offer', 'modal', 'sort', 'BEMHTML'],
	function(provide, bemDom, channels, Button, Offer, Modal, Sort, BEMHTML) {

provide(bemDom.declBlock(this.name, {

	onSetMod: {
        js: {
        	inited: function() {
        		this._maxViewed = this.params.count;
        		this._currentOffers = this.params.offers;

	            this._events(this.findChildBlock({
	            	block: Button,
	            	modName: 'type',
	            	modVal: 'more'
	            })).on('click', this._onMoreClick, this);

	            this._sort = this.findChildBlock(Sort);
	            this._proposeCartModal = this.findChildBlock({
	            	block: Modal,
	            	modName: 'type',
	            	modVal: 'propose-cart'
	            });

              this._domEvents(this._proposeCartModal.findChildBlock({
                block: Button,
                modName: 'type',
                modVal: 'goback'
              }).domElem)
                    .on('pointerclick', this._onGoBackButtonClick, this);

              this._domEvents(this._proposeCartModal.findChildBlock({
                block: Button,
                modName: 'type',
                modVal: 'gocart'
              }).domElem)
                    .on('pointerclick', this._onGoCartButtonClick, this);

	            this._events(this._sort).on('change', this._showOffersWithSortDataAndCurrentFilters, this);
	            this._events(Offer).on('add', this._onOfferAddInCart, this);
	            channels('filters').on('change', this._onFiltersChange, this);
	        }
        }
    },

    _onMoreClick: function() {
   		bemDom.append(
   			this.findChildElem('offers').domElem,
   			BEMHTML.apply(this._currentOffers.slice(this._maxViewed).map(function(item) {
   				return {
   					block: 'offer',
   					mods: { view: 'default' },
   					data: item
   				};
   			}))
   		);

   		this._maxViewed += this.params.count;

   		this.setMod('more', this._currentOffers.length > this._maxViewed ? true : false);
    },

    _showOffersWithSortDataAndCurrentFilters: function() {
    	var sortData = this._sort.getVal();
    	var offers = this.params.offers
    		.filter(function(offer) {
    			var filters = this._filters || [];
    			var result = true;
          debugger;
    			filters.forEach(function(filter) {
    				if (filter.data.indexOf(offer[filter.field].toString()) === -1) {
    					result = false;
    				}
    			});

    			return result;
    		}.bind(this));

    	if (this.__self._sortFuncs[sortData.sort]) {
    		offers = offers.sort(this.__self._sortFuncs[sortData.sort]);
    	}

    	this._currentOffers = offers;

    	bemDom.replace(
   			this.findChildElem('offers').domElem,
   			BEMHTML.apply({
   				block: 'catalog',
   				elem: 'offers',
   				offers: offers,
   				count: sortData.count
   			})
   		);

   		this._maxViewed = sortData.count;

   		this.setMod('more', offers.length > this._maxViewed ? true : false);
    },

    _onFiltersChange: function(e, data) {
    	this._filters = data;

    	this._showOffersWithSortDataAndCurrentFilters();
    },

    _onOfferAddInCart: function(e, data) {
    	this._proposeCartModal.setMod('visible', true);

    	channels('cart').emit('change', data);
    },

    _onGoBackButtonClick: function() {
    	this._proposeCartModal.delMod('visible');
    },

    _onGoCartButtonClick: function() {
    	location.href = '/cart';
    }

}, {

	_sortFuncs: {
		'1': null,
		'2': function(a, b) {
			return Number(b.price) - Number(a.price);
		},
		'3': function(a, b) {
			return Number(a.price) - Number(b.price);
		},
		'4': function(a, b) {
			return (a.vendor + a.model).localeCompare(b.vendor + b.model);
		},
		'5': function(a, b) {
			return a.color.localeCompare(b.color);
		}
	}

}));

});