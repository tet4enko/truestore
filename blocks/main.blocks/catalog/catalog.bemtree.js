block('catalog')(

	content()(function() {
		return [
			{
				block: 'sort',
				count: this.ctx.count
			},
			{
				elem: 'offers',
				offers: this.data.offers,
				count: this.ctx.count
			},
			{
				elem: 'more',
				content: {
					block: 'button',
					mods: { size: 'l', theme: 'islands', type: 'more' },
					text: 'Показать ещё'
				}
			},
			{
			    block : 'modal',
			    mods : { theme : 'islands', autoclosable : true, type: 'propose-cart' },
			    mix: { block: 'catalog', elem: 'propose-cart-modal' },
			    content : [
			    	{
			    		elem: 'propose-cart-label',
			    		content: 'Товар добавлен в корзину!'
			    	},
			    	{
			    		elem: 'propose-cart-buttons',
			    		content: [
			    			{
			    				block: 'button',
			    				mods: { theme: 'islands', size: 'm', type: 'goback' },
			    				text: 'Вернуться к покупкам'
			    			},
			    			{
			    				block: 'button',
			    				mods: { theme: 'islands', size: 'm', view: 'action', type: 'gocart' },
			    				text: 'Перейти к оформлению'
			    			}
			    		]
			    	}
			    ]
			}
		];
	})

);
