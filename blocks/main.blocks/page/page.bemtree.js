block('page')(

	elem('ico')(

		tag()('link'),

		attrs()(function() {
			return {
				rel: 'icon',
				type: 'image/ico',
				href: this.ctx.url,
				sizes: this.ctx.sizes
			};
		})

	),

	def().match(function() {
		return !this.ctx._wrapped;
	})(function() {
		var env = this.env;

		return applyCtx({
			block: this.block,
			mods: this.ctx.mods,
			_wrapped: true,
			title: apply('title'),
			head: [
				{
	                elem: 'css',
	                url: env.hosts.staticHost.prefix + '/_common.css?' + env.cache_version
	            },
	            {
	                elem: 'ico',
	                tag: 'link',
	                attrs: {
						rel: 'icon',
						type: 'image/x-icon',
						href: env.hosts.staticHost.prefix + '/true-16.ico?' + env.cache_version,
						sizes: '16x16'
					}
	            },
	            {
	                elem: 'ico',
	                tag: 'link',
	                attrs: {
						rel: 'icon',
						type: 'image/ico',
						href: env.hosts.staticHost.prefix + '/true-16.ico?' + env.cache_version,
						sizes: '16x16'
					}
	            },
	            {
	                elem: 'ico',
	                tag: 'link',
	                attrs: {
						rel: 'icon',
						type: 'image/ico',
						href: env.hosts.staticHost.prefix + '/true-32.ico?' + env.cache_version,
						sizes: '32x32'
					}
	            },
	            {
	                elem: 'ico',
	                tag: 'link',
	                attrs: {
						rel: 'icon',
						type: 'image/ico',
						href: env.hosts.staticHost.prefix + '/true-64.ico?' + env.cache_version,
						sizes: '64x64'
					}
	            },
	            { tag: 'meta', attrs: { name : 'Keywords', content: apply('keywords') } },
	            { tag: 'meta', attrs: { name : 'Description', content: apply('description') } },

	            { tag: 'meta', attrs: { property : 'og:title', content: apply('og-title') } },
	            { tag: 'meta', attrs: { property : 'og:description', content: apply('og-description') } },
	            { tag: 'meta', attrs: { property : 'og:image', content: apply('og-image') } },
	            { tag: 'meta', attrs: { property : 'og:type', content: 'website' } },
	            { tag: 'meta', attrs: { property : 'og:url', content: apply('og-url') } },

	            { tag: 'meta', attrs: { name: 'yandex-verification', content: 'e81038cd270d8d4a' } },
	            { elem: 'js', url: 'https://www.google.com/recaptcha/api.js' }
			],
			scripts: [
				{ elem: 'js', url: env.hosts.staticHost.prefix + '/_common.js?' + env.cache_version }
			],
			content: [
				{
					elem: 'header'
				},
				{
					elem: 'content'
				},
				{
					elem: 'footer'
				}
	        ]
		});
	})

);
