block('page').mod('type', 'admin')(

	mode('title')(function() {
		return 'Административная панель';
	}),

	elem('header').content()(function() {
		return { block: 'admin-header' };
	}),

	elem('content').content()(function() {
		return {
			block: 'admin-content',
			mods: { view: this.data.view }
		};
	})

);
