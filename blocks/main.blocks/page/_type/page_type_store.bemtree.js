block('page').mod('type', 'store')(

	mode('title')(function() {
		return 'TRUE-STORE — ' + this.data.title;
	}),

	mode('keywords')(function() {
		return this.data.meta && this.data.meta.keywords;
	}),

	mode('description')(function() {
		return this.data.meta && this.data.meta.description;
	}),

	mode('og-title')(function() {
		return this.data.meta && this.data.meta.ogtitle;
	}),

	mode('og-description')(function() {
		return this.data.meta && this.data.meta.ogdescription;
	}),

	mode('og-image')(function() {
		return this.data.meta && this.data.meta.ogimage;
	}),

	mode('og-url')(function() {
		return this.data.meta && this.data.meta.ogurl;
	}),

	elem('header').replace()(() => {
		return { block: 'header' };
	}),

	elem('content').replace()(function() {
		return {
			block: 'store-content',
			js: { offers: this.data.offers },
			mods: { view: this.data.view }
		};
	})

);
