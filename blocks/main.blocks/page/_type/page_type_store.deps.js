({
    shouldDeps: [
        { block: 'header' },
        { block: 'store-content', mods: { view: ['catalog', 'cart', 'delivery', 'feedback', 'offer', 'contacts'] } }
    ]
});
