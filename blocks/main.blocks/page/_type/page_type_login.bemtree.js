block('page').mod('type', 'login')(

	mode('title')(function() {
		return 'Вход';
	}),

	elem('content').content()(function() {
		return { block: 'login-form' };
	})

);
