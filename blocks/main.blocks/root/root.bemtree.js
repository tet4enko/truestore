block('root').replace()(function() {
    this.env = this.ctx.env;
    this.data = this.ctx.data;

    return {
        block : 'page',
        mods: { theme: 'islands', type: this.data.pageType }
    };
})
