block('admin-content').mod('view', 'sales')(

	elem('view').content()(function() {;
		return {
			block: 'admin-sales',
			offers: this.data.offers
		};
	})

);
