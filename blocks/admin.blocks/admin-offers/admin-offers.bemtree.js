block('admin-offers')(

	content()(function() {
		return [
			{
				elem: 'items',
				content: this.data.offers.map(function(offer) {
					return {
						block: 'admin-offer',
						data: offer
					}
				}) 
			},
			{
				elem: 'buttons',
				content: [
					{
						block: 'button',
						mods: { theme: 'islands', size: 'l', type: 'add-offer' },
						text: 'Добавить предложение'
					},
					{
						block: 'button',
						mods: { theme: 'islands', size: 'l', type: 'get-price' },
						text: 'Скачать прайс'
					}
				]
			}
		]
	})

);
