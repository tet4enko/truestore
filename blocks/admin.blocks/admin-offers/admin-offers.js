modules.define('admin-offers', ['i-bem-dom', 'button', 'admin-offer', 'jquery', 'BEMHTML'], function(provide, bemDom, Button, AdminOffer, $, BEMHTML) {

provide(bemDom.declBlock(this.name, {

	onSetMod: {
        js: {
        	inited: function() {
	            this._events(this.findChildBlock({
	            	block: Button,
	            	modName: 'type',
	            	modVal: 'add-offer'
	            })).on('click', this._addOffer, this);

	            this._events(this.findChildBlock({
	            	block: Button,
	            	modName: 'type',
	            	modVal: 'get-price'
	            })).on('click', this._getPrice, this);

                this._events(AdminOffer).on('copy', this._onOfferCopy, this);
	        }
        }
    },

    _addOffer: function() {
    	bemDom.append(this.findChildElem('items').domElem, BEMHTML.apply({
    		block: 'admin-offer',
    		mods: { new: true },
    		data: { name: 'Новое предложение' }
    	}));
    },

    _getPrice: function() {
    	$.ajax({
    		method: 'GET',
    		url: '/admin/offers/price',
            success: this._onGetPriceSuccess.bind(this),
            error: this._onGetPriceError.bind(this)
    	});
    },

    _onGetPriceSuccess: function(data) {
        if (data.status = 'success') {
            window.open(data.path);
        } else {
            this._onGetPriceError(data.error);
        }
    },

    _onGetPriceError: function(err) {
        console.error(err);
        alert('Не удалось получить прайс:(');
    },

    _onOfferCopy: function(event, data) {
        bemDom.append(this.findChildElem('items').domElem, BEMHTML.apply({
            block: 'admin-offer',
            mods: { new: true },
            data: data
        }));
    }

}));

});