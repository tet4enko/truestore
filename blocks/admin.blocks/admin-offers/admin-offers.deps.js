({
	mustDeps: ['i-bem-dom'],
    shouldDeps: [
    	{ block: 'admin-offer', mods: { new: true } },
    	{ block: 'button', mods: { theme: 'islands', size: 'l' } }
    ]
});
