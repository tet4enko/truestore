block('admin-offer')(

	js()(function() {
		return { data: this.ctx.data }
	}),

	content()(function() {
		var data = this.ctx.data || {};

		return [
			{
				elem: 'label',
				content: {
					block: 'link',
					mods: { theme: 'islands', size: 'l', pseudo: true },
					content: data.name
				}
			},
			{
				elem: 'content',
				content: [
					{
						elem: 'fields',
						content: [
							{
								block: 'admin-field',
								mods: { type: 'string' },
								name: 'name',
								label: 'Название',
								val: data.name || ''
							},
							{
								block: 'admin-field',
								mods: { type: 'string' },
								name: 'vendor',
								label: 'Производитель',
								val: data.vendor || ''
							},
							{
								block: 'admin-field',
								mods: { type: 'string' },
								name: 'model',
								label: 'Модель',
								val: data.model || ''
							},
							{
								block: 'admin-field',
								mods: { type: 'string' },
								name: 'internalMemory',
								label: 'Встроенная память',
								val: data.internalMemory
							},
							{
								block: 'admin-field',
								mods: { type: 'string' },
								name: 'color',
								label: 'Цвет',
								val: data.color || ''
							},
							{
								block: 'admin-field',
								mods: { type: 'string' },
								name: 'colorHex',
								label: 'Код цвета (HEX)',
								val: data.colorHex || ''
							},
							{
								block: 'admin-field',
								mods: { type: 'string' },
								name: 'vendorCode',
								label: 'Артикул',
								val: data.vendorCode || ''
							},
							{
								block: 'admin-field',
								mods: { type: 'picture' },
								name: 'pictures',
								label: 'Изображения',
								val: {
									id: data._id,
									count: data.pictures
								}
							},
							{
								block: 'admin-field',
								mods: { type: 'string' },
								name: 'description',
								label: 'Описание',
								val: data.description || ''
							},
							{
								block: 'admin-field',
								mods: { type: 'string' },
								name: 'tag',
								label: 'Тэг (будет открываться по /offers/<тэг>)',
								val: data.tag || ''
							},
							{
								block: 'admin-field',
								mods: { type: 'boolean' },
								name: 'available',
								label: 'Наличие',
								val: data.available
							},
							{
								block: 'admin-field',
								mods: { type: 'string' },
								name: 'price',
								label: 'Цена',
								val: data.price || ''
							},
							{
								block: 'admin-field',
								mods: { type: 'select' },
								name: 'currency',
								label: 'Валюта',
								val: data.currency || 'RUR',
								controlOptions: {
									options: [
								        { val : 'RUR', text : 'Рубли' },
								        { val : 'USD', text : 'Доллары' }
								    ]
								}
							},
							{
								block: 'admin-field',
								mods: { type: 'boolean' },
								name: 'popular',
								label: 'Популярно ли',
								val: data.popular
							}
						]
					},
					{
						elem: 'separator'
					},
					{
						elem: 'buttons',
						content: [
							{
								block: 'button',
								mods: { theme: 'islands', size: 'm', type: 'remove' },
								text: 'Удалить'
							},
							{
								block: 'button',
								mods: { theme: 'islands', size: 'm', type: 'copy' },
								text: 'Копировать'
							},
							{
								elem: 'right-buttons',
								content: [
									{
									    block : 'spin',
									    mods : { theme : 'islands', size : 'xs', type: 'save' }
									},
									{
										block: 'button',
										mods: { theme: 'islands', view: 'action', size: 'm', type: 'save' },
										text: 'Сохранить'
									}
								]
							}
						]
					}
				]
			}
		];
	})

)
