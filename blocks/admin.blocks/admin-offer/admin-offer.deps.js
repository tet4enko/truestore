({
	mustDeps: ['i-bem-dom'],
    shouldDeps: [
    	{ mods: { open: true } },
    	{ block: 'admin-field', mods: { type: ['string', 'boolean', 'select', 'picture'] } },
    	{ block: 'button', mods: { theme: 'islands', view: 'action', size: 'm' } },
    	{ block: 'spin', mods: { theme: 'islands', size: 'xs', visible: true } },
    	{ block: 'link', mods: { theme: 'islands', size: 'l', pseudo: true } }
    ]
});
