modules.define('admin-offer', ['i-bem-dom', 'button', 'spin', 'jquery', 'link', 'admin-field'],
	function(provide, BEMDOM, Button, Spin, $, Link, AdminField) {

	provide(BEMDOM.declBlock(this.name, {

	    onSetMod: {
	        js: {
	        	inited: function() {
		            this._init();
		            this._bindEvents();
		        }
	        },
	        saving: {
	        	true: function() {
	        		this._saveButton.setMod('disabled', true);
	        		this._saveSpin.setMod('visible', true);
	        		this._removeButton.setMod('disabled', true);
	        	},
	        	'': function() {
	        		this._saveButton.delMod('disabled');
	        		this._saveSpin.delMod('visible');
	        		this._removeButton.delMod('disabled');
	        	}
	        }
	    },

	    _init: function() {
	    	this._saveButton = this.findChildBlock({
	    		block: Button,
	    		modName: 'type',
	    		modVal: 'save'
	    	});

	    	this._saveSpin = this.findChildBlock({
	    		block: Spin,
	    		modName: 'type',
	    		modVal: 'save'
	    	});

	    	this._removeButton = this.findChildBlock({
	    		block: Button,
	    		modName: 'type',
	    		modVal: 'remove'
	    	});

	    	this._copyButton = this.findChildBlock({
	    		block: Button,
	    		modName: 'type',
	    		modVal: 'copy'
	    	});
	    },

	    _bindEvents: function() {
	    	this._events(this._saveButton).on('click', this._save, this);
	    	this._events(this._removeButton).on('click', this._remove, this);
	    	this._events(this._copyButton).on('click', this._copy, this);
	    	this._domEvents('label').on('click', this._onLabelClick, this);
	    },

	    _onLabelClick: function() {
	    	this.toggleMod('open');
	    },

	    _save: function() {
	    	this.setMod('saving', true);

	    	$.ajax({
	    		method: 'POST',
	    		url: '/admin/offers/' + (this.getMod('new') ? 'create' : 'save'),
 	    		data: this._getData(),
 	    		processData: false,
				contentType: false,
	    		success: this._onSaveSuccess.bind(this),
	    		error: this._onSaveError.bind(this)
	    	});
	    },

	    _onSaveSuccess: function(data) {
	    	if (data.status === 'success') {
		    	this
		    		.delMod('saving')
		    		.delMod('new');

		    	BEMDOM.update(this.findChildElem('label').findChildBlock(Link).domElem, data.offer.name);

		    	this.params.data = data.offer;
	    	} else {
	    		this._onSaveError("status !== 'success'")
	    	}
	    },

	    _onSaveError: function(err) {
	    	this.delMod('saving');
	    	alert('Не удалось сохранить предложение :(');
	    	console.error(err);
	    },

	    _getData: function() {
	    	var form = new FormData();

	    	if (this.params.data && this.params.data._id) {
	    		form.append('_id', this.params.data._id);
	    	}

	    	this.findChildBlocks(AdminField).forEach(function(field) {
	    		form.append(field.params.name, field.val ? field.val() : '');
	    	});

	    	return form;
	    },

	    _remove: function() {
	    	if (this.getMod('new')) {
	    		BEMDOM.destruct(this.domElem);
	    	} else if (confirm('Вы действительно хотите удалить ' + this.params.data.name + ' ?')) {
	    		$.ajax({
		    		method: 'POST',
		    		url: '/admin/offers/remove',
	 	    		data: { id: this.params.data._id },
		    		success: this._onRemoveSuccess.bind(this),
		    		error: this._onRemoveError.bind(this)
		    	});
	    	}
	    },

	    _onRemoveSuccess: function(data) {
	    	if (data.status === 'success') {
	    		BEMDOM.destruct(this.domElem);
	    	} else {
	    		this._onRemoveError("status !== 'success'");
	    	}
	    },

	    _onRemoveError: function(err) {
	    	alert('Не удалось удалить предложение :(');
	    	console.error(err);
	    },

	    _copy: function() {
	    	this._emit('copy', this.params.data);
	    }

	}));

});
