block('admin-order')(

	def().match(function() { return !this.ctx._wrap; })(function() {
		var ctx = this.ctx;
		var mods = ctx.mods || {};

		mods.status = this.ctx.data.status;

		ctx.mods = mods;
		ctx._wrap = true;

		return applyCtx(ctx);
	}),


	js()(function() {
		return { data: this.ctx.data }
	}),

	content()(function() {
		var data = this.ctx.data || {};
	
		return [
			{
				elem: 'field',
				content: [
					{
						elem: 'field-label',
						content: 'ID заказа:'
					},
					{
						elem: 'field-content',
						content: data._id.toString()
					}
				]
			},
			{
				elem: 'field',
				content: [
					{
						elem: 'field-label',
						content: 'Телефон:'
					},
					{
						elem: 'field-content',
						content: data.phone
					}
				]
			},
			{
				elem: 'field',
				content: [
					{
						elem: 'field-label',
						content: 'Тип доставки:'
					},
					{
						elem: 'field-content',
						content: data.deliveryType
					}
				]
			},
			{
				elem: 'field',
				content: [
					{
						elem: 'field-label',
						content: 'Комментарий:'
					},
					{
						elem: 'field-content',
						content: data.comment
					}
				]
			},
			{
				elem: 'field',
				content: [
					{
						elem: 'field-label',
						content: 'Сумма (без доставки):'
					},
					{
						elem: 'field-content',
						content: data.sum + ' руб'
					}
				]
			},
			{
				elem: 'field',
				content: [
					{
						elem: 'field-label',
						content: 'Товары'
					},
					{
						elem: 'field-content',
						tag: 'ol',
						attrs: { style: 'display: block;' },
						content: data.items.map(function(item) {
							return {
								elem: 'admin-order-item',
								data: item
							};
						})
					}
				]
			},
			{
				elem: 'field',
				content: [
					{
						elem: 'field-label',
						content: 'Создан:'
					},
					{
						elem: 'field-content',
						content: new Date(data.created).toLocaleString()
					}
				]
			},
			{
				elem: 'field',
				mix: { elem: 'last-status-change' },
				content: [
					{
						elem: 'field-label',
						content: 'Последнее изменение статуса:'
					},
					{
						elem: 'field-content',
						content: new Date(data.lastStatusChange).toLocaleString()
					}
				]
			},
			{
				elem: 'buttons',
				canRemove: this.ctx.canRemove
			}
		];
	}),

	elem('admin-order-item')(

		tag()('li'),

		content()(function() {
			var data = this.ctx.data;

			return data.offerName + ' ' + data.offerColor + ', по цене ' + data.offerPrice + ' руб, ' + data.offerCount + ' штук'
		})
	)

)
