block('admin-order').mod('status', 'closed')(

	elem('buttons').content()(function() {

		return this.ctx.canRemove ? {
			block: 'button',
			mods: { theme: 'islands', size: 'm', type: 'remove' },
			text: 'Удалить'
		} : '';
	})

);
