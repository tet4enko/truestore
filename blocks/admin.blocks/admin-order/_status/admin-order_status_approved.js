modules.define('admin-order', ['i-bem-dom', 'button', 'jquery', 'BEMHTML'], function(provide, bemDom, Button, $, BEMHTML, AdminOrder) {

	provide(AdminOrder.declMod({ modName : 'status', modVal : 'approved' }, {

		onSetMod: {
	        js: {
	        	inited: function() {
	        		this._init();
		        }
	        },
	        status: {
	        	closed: function() {
	        		bemDom.update(this.findChildElem('buttons').domElem, '');
	        	}
	        }
	    },

		_init: function() {
			this._events(this.findChildBlock({
                block: Button,
                modName: 'type',
                modVal: 'close'
            })).on('click', this._onCloseButtonClick, this);

            this._events(this.findChildBlock({
                block: Button,
                modName: 'type',
                modVal: 'cancel'
            })).on('click', this._onCancelButtonClick, this);
		},

		_onCloseButtonClick: function() {
	        $.ajax({
	            method: 'POST',
	            url: '/admin/orders/close',
	            data: { id: this.params.data._id },
	            success: this._onCloseSuccess.bind(this),
	            error: this._onCloseError.bind(this)
	        });
	    },

	    _onCloseSuccess: function(data) {
	        if (data.status === 'success') {
	        	bemDom.update(
	            	this.findChildElem('last-status-change').findChildElem('field-content').domElem,
	            	new Date(data.order.lastStatusChange).toLocaleString()
	            );
	            this.setMod('status', 'closed');
	        }  else {
	            this._onApproveError("status !== success");
	        }
	    },

	    _onCloseError: function(err) {
	        console.error(err);
	        alert('Не удалось закрыть заказ:(');
	    },

	    _onCancelButtonClick: function() {
	    	if (confirm('Отменить заказ?')) {
	    		$.ajax({
		            method: 'POST',
		            url: '/admin/orders/cancel',
		            data: { id: this.params.data._id },
		            success: this._onCancelSuccess.bind(this),
		            error: this._onCancelError.bind(this)
		        });
	    	}
	    },

	    _onCancelSuccess: function(data) {
	    	if (data.status === 'success') {
	            bemDom.destruct(this.domElem);
	        }  else {
	            this._onCancelError("status !== success");
	        }
	    },

	    _onCancelError: function(err) {
	    	console.error(err);
	        alert('Не удалось отменить заказ:(');
	    }


	}));

});
