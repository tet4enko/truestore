block('admin-order').mod('status', 'new')(

	elem('buttons').content()(function() {

		return [
			{
				block: 'button',
				mods: { theme: 'islands', size: 'm', type: 'approve' },
				text: 'Подтвердить'
			},
			{
				block: 'button',
				mods: { theme: 'islands', size: 'm', type: 'cancel' },
				text: 'Отменить'
			}
		];
	})

);
