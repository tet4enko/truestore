block('admin-order').mod('status', 'approved')(

	elem('buttons').content()(function() {

		return [
			{
				block: 'button',
				mods: { theme: 'islands', size: 'm', type: 'close' },
				text: 'Выполнить'
			},
			{
				block: 'button',
				mods: { theme: 'islands', size: 'm', type: 'cancel' },
				text: 'Отменить'
			}
		];
	})

);
