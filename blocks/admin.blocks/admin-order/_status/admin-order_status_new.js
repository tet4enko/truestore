modules.define('admin-order', ['i-bem-dom', 'button', 'jquery', 'BEMHTML'], function(provide, bemDom, Button, $, BEMHTML, AdminOrder) {

	provide(AdminOrder.declMod({ modName : 'status', modVal : 'new' }, {

		onSetMod: {
	        js: {
	        	inited: function() {
	        		this._events(this.findChildBlock({
	                    block: Button,
	                    modName: 'type',
	                    modVal: 'approve'
	                })).on('click', this._onApproveButtonClick, this);

	                this._events(this.findChildBlock({
	                    block: Button,
	                    modName: 'type',
	                    modVal: 'cancel'
	                })).on('click', this._onCancelButtonClick, this);
		        }
	        },
	        status: {
	        	approved: function() {
	        		bemDom.update(this.findChildElem('buttons').domElem, BEMHTML.apply({
						block: 'button',
						mods: { theme: 'islands', size: 'm', type: 'close' },
						text: 'Выполнить'
					}));

					this._init();
	        	}
	        }
	    },

	    _onApproveButtonClick: function() {
	        $.ajax({
	            method: 'POST',
	            url: '/admin/orders/approve',
	            data: { id: this.params.data._id },
	            success: this._onApproveSuccess.bind(this),
	            error: this._onApproveError.bind(this)
	        });
	    },

	    _onApproveSuccess: function(data) {
	        if (data.status === 'success') {
	            this.setMod('status', 'approved');
	            bemDom.update(
	            	this.findChildElem('last-status-change').findChildElem('field-content').domElem,
	            	new Date(data.order.lastStatusChange).toLocaleString()
	            );
	        }  else {
	            this._onApproveError("status !== success");
	        }
	    },

	    _onApproveError: function(err) {
	        console.error(err);
	        alert('Не удалось подтвердить заказ:(');
	    },

	    _onCancelButtonClick: function() {
	    	if (confirm('Отменить заказ?')) {
	    		$.ajax({
		            method: 'POST',
		            url: '/admin/orders/cancel',
		            data: { id: this.params.data._id },
		            success: this._onCancelSuccess.bind(this),
		            error: this._onCancelError.bind(this)
		        });
	    	}
	    },

	    _onCancelSuccess: function(data) {
	    	if (data.status === 'success') {
	            bemDom.destruct(this.domElem);
	        }  else {
	            this._onCancelError("status !== success");
	        }
	    },

	    _onCancelError: function(err) {
	    	console.error(err);
	        alert('Не удалось отменить заказ:(');
	    }

	}));

});
