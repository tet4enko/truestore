modules.define('admin-order', ['i-bem-dom', 'button', 'jquery', 'BEMHTML'], function(provide, bemDom, Button, $, BEMHTML, AdminOrder) {

	provide(AdminOrder.declMod({ modName : 'status', modVal : 'canceled' }, {

		onSetMod: {
	        js: {
	        	inited: function() {
	        		this._init();
		        }
	        }
	    },

		_init: function() {
			this._events(this.findChildBlock({
                block: Button,
                modName: 'type',
                modVal: 'remove'
            })).on('click', this._onRemoveButtonClick, this);
		},

		_onRemoveButtonClick: function() {
			if (confirm('Точно удалить заказ?')) {
		        $.ajax({
		            method: 'POST',
		            url: '/admin/orders/remove',
		            data: { id: this.params.data._id },
		            success: this._onRemoveSuccess.bind(this),
		            error: this._onRemoveError.bind(this)
		        });
		    }
	    },

	    _onRemoveSuccess: function(data) {
	        if (data.status === 'success') {
	        	bemDom.destruct(this.domElem);
	        }  else {
	            this._onRemoveError("status !== success");
	        }
	    },

	    _onRemoveError: function(err) {
	        console.error(err);
	        alert('Не удалось удалить заказ:(');
	    }


	}));

});
