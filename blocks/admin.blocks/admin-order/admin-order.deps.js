({
	mustDeps: ['i-bem-dom'],
    shouldDeps: [
    	{ mods: { status: ['new', 'approved', 'closed', 'canceled'] } },
    	{ block: 'button', mods: { theme: 'islands', size: 'm' } }
    ]
});
