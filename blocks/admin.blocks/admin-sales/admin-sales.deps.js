({
	mustDeps: ['i-bem-dom'],
    shouldDeps: [
    	{ block: 'admin-sale', mods: { new: true, invalid: true } },
    	{ block: 'button', mods: { theme: 'islands', size: 'l' } }
    ]
});
