block('admin-sales')(

	content()(function() {
		var that = this;

		return [
			{
				elem: 'items',
				content: this.data.sales.map(function(sale) {
					return {
						block: 'admin-sale',
						mods: { invalid: (sale.offerData ? false : true) },
						sale: sale,
						offers: that.data.offers
					}
				}) 
			},
			{
				elem: 'buttons',
				content: [
					{
						block: 'button',
						mods: { theme: 'islands', size: 'l', type: 'add-sale' },
						text: 'Добавить скидку'
					},
					{
						block: 'button',
						mods: { theme: 'islands', size: 'l', type: 'get-price' },
						text: 'Скачать прайс'
					}
				]
			}
		]
	})

);
