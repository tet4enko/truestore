block('admin-field').mod('type', 'string')(

	elem('control').content()(function() {
		return {
			block: 'input',
			mods: { theme: 'islands', size: 'm', 'has-clear': true },
			val: this.ctx.val
		};
	})

);
