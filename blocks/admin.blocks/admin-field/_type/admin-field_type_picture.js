modules.define('admin-field', ['attach', 'jquery', 'BEMHTML', 'i-bem-dom'], function(provide, Attach, $, BEMHTML, bemDom,  AdminOfferField) {

	provide(AdminOfferField.declMod({ modName : 'type', modVal : 'picture' }, {

		onSetMod: {
	        js: {
	        	inited: function() {
	        		this._control = this.findChildBlock(Attach);

	        		//this._events(this._control).on('change', this._onAttachChange, this);
		        }
	        }
	    },

	    val: function() {
	    	return this._control.findChildElem('control').domElem[0].files[0];
	    },

	    /*val: function() {
	    	var pictures = this.findChildElems('picture') || [];

	    	return pictures.map(function(picture) {
	    		return picture.params.url;
	    	});
		},*/

		/*_onAttachChange: function(data) {
			var file = this._control.findChildElem('control').domElem[0].files[0];
			if (file) {
				var form = new FormData();

				form.append('picture', file);

				$.ajax({
					url: '/admin/offers/picture',
					type: 'POST',
					data: form,
					processData: false,
					contentType: false,
					success: this._onFileUploadSuccess.bind(this),
					error: this._onFileUploadError.bind(this)
				});
			}
		},

		_onFileUploadSuccess: function(data) {
			if (data.status === 'success') {
				var path = data.path;

				bemDom.append(this.findChildElem('pictures').domElem, BEMHTML.apply({
					block: 'admin-field',
					elem: 'picture',
					js: { url: path }
				}));
			} else {
				this._onFileUploadError("status !== 'success'");
			}
		},

		_onFileUploadError: function(error) {
			console.error(error);
			alert('Не удалось загрузить файл:(');
		}*/

	}));

});
