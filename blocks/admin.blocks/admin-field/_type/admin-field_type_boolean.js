modules.define('admin-field', ['checkbox'], function(provide, Checkbox, AdminOfferField) {

	provide(AdminOfferField.declMod({ modName : 'type', modVal : 'boolean' }, {

		onSetMod: {
	        js: {
	        	inited: function() {
	        		this._control = this.findChildBlock(Checkbox);
		        }
	        }
	    },

	    val: function() {
			return this._control ? Boolean(this._control.getMod('checked')) : null;
		}

	}));

});
