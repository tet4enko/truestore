modules.define('admin-field', ['select'], function(provide, Select, AdminOfferField) {

	provide(AdminOfferField.declMod({ modName : 'type', modVal : 'select' }, {

		onSetMod: {
	        js: {
	        	inited: function() {
	        		this._control = this.findChildBlock(Select);
		        }
	        }
	    }

	}));

});
