block('admin-field').mod('type', 'select')(

	elem('control').content()(function() {
		return {
		    block : 'select',
		    mods : { mode : 'radio', theme : 'islands', size : 'm' },
		    name : Math.random() * Math.random(),
		    val : this.ctx.val,
		    options : this.ctx.controlOptions.options
		};
	})

);
