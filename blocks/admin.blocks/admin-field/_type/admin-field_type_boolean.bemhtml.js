block('admin-field').mod('type', 'boolean')(

	elem('control').content()(function() {
		return {
			block: 'checkbox',
			mods: { theme: 'islands', size: 'm', checked: this.ctx.val },
			name: Math.random() * Math.random(),
			text: 'Да / Нет'
		};
	})

);
