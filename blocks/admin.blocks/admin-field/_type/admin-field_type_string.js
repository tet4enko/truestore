modules.define('admin-field', ['input'], function(provide, Input, AdminOfferField) {

	provide(AdminOfferField.declMod({ modName : 'type', modVal : 'string' }, {

		onSetMod: {
	        js: {
	        	inited: function() {
	        		this._control = this.findChildBlock(Input);
		        }
	        }
	    }

	}));

});
