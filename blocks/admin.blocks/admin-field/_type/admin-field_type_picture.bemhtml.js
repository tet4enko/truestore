block('admin-field').mod('type', 'picture')(

	elem('control').content()(function() {
		var val = this.ctx.val;
		var id = val.id;
		var count = val.count;
		var picturesContent = [];

		for (var i = 0; i < count; i++) {
			picturesContent.push({
				elem: 'picture',
				js: { url: '/_image?offer=' + id + '&index=' + i }
			});
		}

		return [
			{
				elem: 'pictures',
				content: picturesContent
			},
			{
			    block : 'attach',
			    mods : { theme : 'islands', size : 's', type: 'upload-picture' },
			    button : 'Выберите изображение',
			    noFileText : 'не выбрано'
			}
		];
	})

);
