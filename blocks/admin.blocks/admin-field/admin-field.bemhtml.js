block('admin-field')(

	js()(function() {
		return { name: this.ctx.name };
	}),

	content()(function() {
		return [
			{ elem: 'label', content: this.ctx.label },
			{ elem: 'control', val: this.ctx.val, controlOptions: this.ctx.controlOptions }
		];
	})

);
