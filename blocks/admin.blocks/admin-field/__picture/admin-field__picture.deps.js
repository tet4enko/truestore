[{
    mustDeps : { block : 'i-bem-dom' },
    shouldDeps: [
    	{ block: 'link', mods: { theme: 'islands', size: 's', } },
    	{ block: 'image' },
    	{ block: 'fa' }
    ]
}]
