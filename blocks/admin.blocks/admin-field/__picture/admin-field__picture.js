modules.define('admin-field__picture', ['i-bem-dom'], function(provide, bemDom) {

	provide(bemDom.declElem('admin-field', 'picture', {

		onSetMod: {
			js: function() {
				this._domEvents('picture-remove').on('click', this._onClick, this);
			}
		},

		_onClick: function() {
			bemDom.destruct(this.domElem);
		}

	}));

});
