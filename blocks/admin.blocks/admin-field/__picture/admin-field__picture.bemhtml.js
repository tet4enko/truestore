block('admin-field').elem('picture')(

	js()(true),

	content()(function() {
		return [
			{
				block: 'link',
				mods: { theme: 'islands', size: 's' },
				attrs: { target: '_blank' },
				url: this.ctx.js.url,
				content: {
					block: 'image',
					width: 60,
					url: this.ctx.js.url
				}
			}/*,
			{
				block: 'fa',
				mix: { block: 'admin-field', elem: 'picture-remove' },
				mods: {
					icon: 'close',
					type: 'remove'
				}
			}*/
		];
	})

);
