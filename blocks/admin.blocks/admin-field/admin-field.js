modules.define('admin-field', ['i-bem-dom'], function(provide, bemDom) {

provide(bemDom.declBlock(this.name, {

	val: function() {
		return this._control ? this._control.getVal() : null;
	}

}, {
}));

});