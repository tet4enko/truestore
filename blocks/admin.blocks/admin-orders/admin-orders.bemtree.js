block('admin-orders')(

	content()(function() {

		return [
			{
				elem: 'items',
				content: this.data.orders.map(function(order) {
					return {
						block: 'admin-order',
						data: order,
						canRemove: Boolean(this.data.canRemove)
					}
				}.bind(this)) 
			}
		]
	})

);
