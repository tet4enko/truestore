block('admin-header')(

	content()(function() {
		return [
			{
				elem: 'header-items',
				content: [
					{
						elem: 'header-item',
						content: {
						    block : 'link',
						    mods : { theme : 'islands', size: 'xl' },
						    url : '/admin/offers',
						    content : 'Предложения'
						}
					},
					{
						elem: 'header-item',
						content: {
						    block : 'link',
						    mods : { theme : 'islands', size: 'xl' },
						    url : '/admin/sales',
						    content : 'Акции'
						}
					},
					{
						elem: 'header-item',
						content: {
						    block : 'link',
						    mods : { theme : 'islands', size: 'xl' },
						    url : '/admin/orders',
						    content : 'Заказы'
						}
					}
				]
			},
			{
				elem: 'logout',
				content: {
					block: 'link',
					mods: { theme: 'islands', size: 'm', view: 'minor' },
					url: '/_logout',
					content: 'Выйти'
				}
			}
		];
	})

);
