modules.define('admin-sale', ['i-bem-dom', 'button', 'spin', 'jquery', 'link', 'admin-field'],
	function(provide, BEMDOM, Button, Spin, $, Link, AdminField) {

	provide(BEMDOM.declBlock(this.name, {

	    onSetMod: {
	        js: {
	        	inited: function() {
		            this._init();
		            this._bindEvents();
		        }
	        },
	        saving: {
	        	true: function() {
	        		this._saveButton.setMod('disabled', true);
	        		this._saveSpin.setMod('visible', true);
	        		this._removeButton.setMod('disabled', true);
	        	},
	        	'': function() {
	        		this._saveButton.delMod('disabled');
	        		this._saveSpin.delMod('visible');
	        		this._removeButton.delMod('disabled');
	        	}
	        }
	    },

	    _init: function() {
	    	this._saveButton = this.findChildBlock({
	    		block: Button,
	    		modName: 'type',
	    		modVal: 'save'
	    	});

	    	this._saveSpin = this.findChildBlock({
	    		block: Spin,
	    		modName: 'type',
	    		modVal: 'save'
	    	});

	    	this._removeButton = this.findChildBlock({
	    		block: Button,
	    		modName: 'type',
	    		modVal: 'remove'
	    	});
	    },

	    _bindEvents: function() {
	    	this._saveButton && this._events(this._saveButton).on('click', this._save, this);
	    	this._removeButton && this._events(this._removeButton).on('click', this._remove, this);
	    },

	    _save: function() {
	    	this.setMod('saving', true);

	    	$.ajax({
	    		method: 'POST',
	    		url: '/admin/sales/' + (this.getMod('new') ? 'create' : 'save'),
 	    		data: this._getData(),
	    		success: this._onSaveSuccess.bind(this),
	    		error: this._onSaveError.bind(this)
	    	});
	    },

	    _onSaveSuccess: function(data) {
	    	if (data.status === 'success') {
		    	this
		    		.delMod('saving')
		    		.delMod('new');

		    	this.params.data = data.sale;
	    	} else {
	    		this._onSaveError("status !== 'success'")
	    	}
	    },

	    _onSaveError: function(err) {
	    	this.delMod('saving');
	    	alert('Не удалось сохранить предложение :(');
	    	console.error(err);
	    },

	    _getData: function() {
	    	var data = {};

	    	if (this.params.data && this.params.data._id) {
	    		data._id = this.params.data._id;
	    	}

	    	this.findChildBlocks(AdminField).forEach(function(field) {
	    		data[field.params.name] = field.val ? field.val() : '';
	    	});

	    	return data;
	    },

	    _remove: function() {
	    	if (this.getMod('new')) {
	    		BEMDOM.destruct(this.domElem);
	    	} else if (confirm('Вы действительно хотите удалить данную акцию ?')) {
	    		$.ajax({
		    		method: 'POST',
		    		url: '/admin/sales/remove',
	 	    		data: { id: this.params.data._id },
		    		success: this._onRemoveSuccess.bind(this),
		    		error: this._onRemoveError.bind(this)
		    	});
	    	}
	    },

	    _onRemoveSuccess: function(data) {
	    	if (data.status === 'success') {
	    		BEMDOM.destruct(this.domElem);
	    	} else {
	    		this._onRemoveError("status !== 'success'");
	    	}
	    },

	    _onRemoveError: function(err) {
	    	alert('Не удалось удалить акцию :(');
	    	console.error(err);
	    }

	}));

});
