({
    shouldDeps: [
    	{ block: 'admin-field', mods: { type: ['string', 'select'] } },
    	{ block: 'button', mods: { theme: 'islands', view: 'action', size: 'm' } },
    	{ block: 'spin', mods: { theme: 'islands', size: 'xs', visible: true } }
    ]
});
