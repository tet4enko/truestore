block('admin-sale').mod('invalid', true)(

	content()(function() {
		var data = this.ctx.sale || {};
		var offers = this.ctx.offers;

		return [
			{
				elem: 'fields',
				content: 'Предложение акции было удалено.'
			},
			{
				elem: 'separator'
			},
			{
				elem: 'buttons',
				content: [
					{
						block: 'button',
						mods: { theme: 'islands', size: 'm', type: 'remove' },
						text: 'Удалить'
					}
					
				]
			}
		];
	})

);
