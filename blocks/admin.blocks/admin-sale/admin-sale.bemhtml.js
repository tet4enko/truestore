block('admin-sale')(

	js()(function() {
		return { data: this.ctx.sale }
	}),

	content()(function() {
		var data = this.ctx.sale || {};
		var offers = this.ctx.offers;

		return [
			{
				elem: 'fields',
				content: [
					{
						block: 'admin-field',
						mods: { type: 'select' },
						name: 'offerId',
						label: 'Предложение',
						val: data.offerId || null,
						controlOptions: {
							options: offers.map(function(offer) {
								return {
									val: offer._id.toString(),
									text: offer.name + ' (' + offer.price + ' рублей)'
								};
							})
						}
					},
					{
						block: 'admin-field',
						mods: { type: 'string' },
						name: 'newPrice',
						label: 'Новая цена',
						val: data.newPrice || '0'
					}
				]
			},
			{
				elem: 'separator'
			},
			{
				elem: 'buttons',
				content: [
					{
						block: 'button',
						mods: { theme: 'islands', size: 'm', type: 'remove' },
						text: 'Удалить'
					},
					{
						elem: 'right-buttons',
						content: [
							{
							    block : 'spin',
							    mods : { theme : 'islands', size : 'xs', type: 'save' }
							},
							{
								block: 'button',
								mods: { theme: 'islands', view: 'action', size: 'm', type: 'save' },
								text: 'Сохранить'
							}
						]
					}
				]
			}
		];
	})

)
