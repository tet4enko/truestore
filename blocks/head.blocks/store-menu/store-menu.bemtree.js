block('store-menu')(

	content()(function() {
		var view = this.data.view;
		var items = [
			{ label: 'Каталог', viewName: 'catalog', url: '/catalog' },
			{ label: 'Доставка и оплата', viewName: 'delivery', url: '/delivery' },
			{ label: 'Обратная связь', viewName: 'feedback', url: '/feedback' },
			{ label: 'Контакты', viewName: 'contacts', url: '/contacts' }
		];

		return items.map(function(item) {
			return {
				block: 'link',
				mods: { theme: 'islands', view: 'text' },
				url: item.url,
				content: {
					block: 'store-menu',
					elem: 'item',
					elemMods: { active: view === item.viewName },
					content: item.label
				}
			}
		});
	})

);
