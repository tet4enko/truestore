modules.define('cart', ['i-bem-dom', 'events__channels', 'i-localstorage'], function(provide, bemDom, channels, iLocalstorage) {

provide(bemDom.declBlock(this.name, {

	onSetMod: {
        js: {
        	inited: function() {
                this._clear();

                this._update();
                this.setMod('visible', true);

                channels('cart').on('change', this._update, this);
	        }
        }
    },

    _update: function() {
        var cartData = iLocalstorage.getItem('cart');
        var products = 0;
        var sum = 0;

        cartData = cartData ? JSON.parse(cartData) : {};

        Object.keys(cartData).forEach(function(offerId) {
            var offer = this.params.offers.filter(function(item) {
                return item._id === offerId;
            })[0];

            if (offer) {
                products += cartData[offerId];
                sum += Number(offer.price);
            }
        }, this);

        this.setMod('not-empty', products ? true : false);

        bemDom.update(this.findChildElem('count').domElem, products);
    },

    _clear: function() {
        var cartData = iLocalstorage.getItem('cart');
        var newData = {};

        cartData = cartData ? JSON.parse(cartData) : {};

        Object.keys(cartData).forEach(function(offerId) {
            var offer = this.params.offers.filter(function(item) {
                return item._id === offerId;
            })[0];

            if (offer) {
                newData[offerId] = cartData[offerId];
            }
        }, this);

        iLocalstorage.setItem('cart', JSON.stringify(newData));
    }

}));

});