({
	mustDeps: [
		'i-bem-dom',
		{
            block: 'events',
            elem: 'channels'
        },
        { block: 'i-localstorage' }
	],
    shouldDeps: [
    	{ mods: { visible: true, 'not-empty': true } },
        { block: 'fa' },
        { block: 'link', mods: { theme: 'islands', size: 'm' } }
    ]
});
