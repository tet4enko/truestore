block('cart')(

	js()(function() {
		return { offers: this.ctx.offers };
	}),

	content()(function() {
		return [
			{
				block: 'link',
				mods: { theme: 'islands', size: 'm' },
				url: '/cart',
				title: 'Перейти в Корзину',
				content: {
					block: 'fa',
					mix: { block: 'cart', elem: 'cart-icon' },
					mods: { icon: 'shopping-cart' },
					content: {
						block: 'cart',
						elem: 'count',
						content: '5'
					}
				}
			}
		];
	})

);
