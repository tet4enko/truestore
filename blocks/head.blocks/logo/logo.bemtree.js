block('logo')(

	content()(function() {
		return [
			{
				elem: 'top-line',
				attrs: {
					itemprop: 'name',
					content: 'TRUE-STORE'
				},
				content: 'TRUE-STORE'
			},
			{
				elem: 'bottom-line',
				content: 'лучшие смартфоны по лучшей цене'
			},
			{
				tag: 'meta',
				attrs: {
					itemprop: 'telephone',
					content: '+79780241117'
				}
			},
			{
				tag: 'meta',
				attrs: {
					itemprop: 'email',
					content: 'thisistruestore@gmail.com'
				}
			},
			{
				block: 'address',
				attrs: {
					itemprop: 'address',
					itemscope: '',
					itemtype: 'https://schema.org/PostalAddress'
				},
				content: [
					{
						tag: 'meta',
						attrs: {
							itemprop: 'streetAddress',
							content: 'Объездная улица, 10 (Радиорынок). Бутик № а25/a26'
						}
					},
					{
						tag: 'meta',
						attrs: {
							itemprop: 'addressLocality',
							content: 'Симферополь'
						}
					}
				]
			},
		];
	}),

	attrs()({
		itemscope: '',
		itemtype: 'https://schema.org/Organization'
	})

);
