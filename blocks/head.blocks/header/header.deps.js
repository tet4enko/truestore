({
	mustDeps: 'i-bem-dom',
    shouldDeps: [
        { block: 'logo' },
        { block: 'store-menu' },
        { block: 'cart' }
    ]
});
