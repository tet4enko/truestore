block('header')(

	content()(function() {
		return [
			{
				elem: 'top-line',
				content: [
					{
						block: 'logo'
					},
					{
						elem: 'right-info',
						content: { block: 'cart', offers: this.data.offers }
					}
				]
			},
			{
				block: 'store-menu'
			}
		];	
	})

);
