'use strict';

module.exports = {
    app: {
        env: 'development',
        port: 8080
    },

    mongoose: {
        url: 'mongodb://127.0.0.1:27017/test1',
        debug: true
    },

    logger: [
        {
            name: 'Console',
            options: { level: 'debug' }
        },
        {
            name: 'File',
            options: { level: 'debug', filename: 'server.log' }
        }
    ],

    session: {
        secret: "supersecret",
        key: "sessionid",
        cookie: {
          path: "/",
          httpOnly: true,
          maxAge: null
        }
    },

    hosts: {
        staticHost: {
            prefix: '/_m',
            bundlesPath: 'desktop.bundles/common/'
        }
    },

    email: {
        sender: {
            address: 'truestoresender@gmail.com',
            password: 'fuckfuckfuck'
        },
        orders: {
            address: 'truestoreorders@gmail.com,tetchenko@gmail.com'
        },
        feedback: {
            address: 'truestoreorders@gmail.com,tetchenko@gmail.com'
        }
    },

    recaptcha: {
        secret: '6LfPdhMUAAAAAG7UYmeWiWbOO2jPytvE72hDgJNF',
        public: '6LfPdhMUAAAAAOrqx-u3gRBJowdBvnngbU7dND72',
        url: 'https://www.google.com/recaptcha/api/siteverify'
    }
};
