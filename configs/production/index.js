'use strict';

var fs = require('fs');
var path = require('path');

module.exports = {
    app: {
        env: 'production',
        port: process.env.PORT || 443
    },

    mongoose: {
        url: 'mongodb://website:website@ds157521.mlab.com:57521/truestore',
        debug: false
    },

    logger: [
        {
            name: 'Console',
            options: { level: 'info' }
        },
        {
            name: 'File',
            options: { level: 'info', filename: 'server.log' }
        }
    ],

    https: {
        private: fs.readFileSync(path.resolve(__dirname, '../../certs/', 'private.key'), 'utf8'),
        certificate: fs.readFileSync(path.resolve(__dirname, '../../certs/', 'certificate.crt'), 'utf8')
    },

    session: {
        secret: "supersecret",
        key: "sessionid",
        cookie: {
          path: "/",
          httpOnly: true,
          maxAge: null
        }
    },

    hosts: {
        staticHost: {
            prefix: '/_m',
            bundlesPath: 'desktop.bundles/common/'
        }
    },

    email: {
        sender: {
            address: 'truestoresender@gmail.com',
            password: 'fuckfuckfuck'
        },
        orders: {
            address: 'truestoreorders@gmail.com,mailfororders1@gmail.com,tetchenko@gmail.com'
        },
        feedback: {
            address: 'truestoreorders@gmail.com,mailtosupport1@yandex.ru,tetchenko@gmail.com'
        }
    },

    recaptcha: {
        secret: '6LfPdhMUAAAAAG7UYmeWiWbOO2jPytvE72hDgJNF',
        public: '6LfPdhMUAAAAAOrqx-u3gRBJowdBvnngbU7dND72',
        url: 'https://www.google.com/recaptcha/api/siteverify'
    }
};
