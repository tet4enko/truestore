'use strict';

var fs = require('fs');
var express = require('express');
var http = require('http');
var https = require('https');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var app = express();
var session = require('express-session');
var config = require('./modules/config');
var renderer = require('./modules/middlewares/renderer');
var routes = require('./modules/middlewares/routes.js');
var environment = require('./modules/middlewares/environment.js');
var logger = require('./modules/logger');
var mongoose = require('./modules/mongoose');

process.on('uncaughtException', function (err) {
    logger.error('Uncaught Exception', err.toString());
});

// TODO вынести это куда-то. Пока не придумал, куда лучше.
function renderErrorTemplate(err, req, res, next) {
    try {
        logger.error('JS Exception.', err.toString());
        logger.warn('Try render default 500 page.');
        // Попробовать отрендерить красивую 500-ку
        res.status(500).send(res.render('bemhtml', res.render('bemtree', {
            block: 'blablabla'
        }))).end();
    } catch (err2) {
        logger.error('Render default 500 page. Catched error:', err2.toString());
        // В самом худшем случае (вообще), отдать просто 500
        res.status(500).send('Render default 500 page crashed.').end();
    }
}

app
    .use('/freeze/', express.static(__dirname +  '/freeze'))
    .use('/_m/', express.static(__dirname +  '/configs/icons'))
    .use('/_m/', express.static(__dirname + '/' + config.hosts.staticHost.bundlesPath))
    .use('/_userimages/', express.static(__dirname +  '/userimages'))
    .use('/_prices/', express.static(__dirname +  '/prices'))
    .use('/_publicpics/', express.static(__dirname +  '/publicpics'))

    .use(bodyParser.json({ limit: '10Mb' }))
    .use(bodyParser.urlencoded({ extended: true, limit: '10Mb' }))
    .use(cookieParser())
    .use(session({ secret: 'ggwp' }))
    .use(renderer)
    .use(environment)
    .use(function(req, res, next) {
        if (req.env.env === 'production') {
            res.set('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload');
        }

        next();
    })
    .use(routes)
    .use(function (err, req, res, next) {
        // если необходимо отрендерить html c 500
        if (req.useErrorTemplate) {
            renderErrorTemplate.apply(this, arguments);
        } else {
            // Обработка JS-исключений для оставшихся роутов
            var statusCode = 500;

            if (err.statusCode) {
                statusCode = err.statusCode; // from body-parser
            }
            console.log(err);
            logger.error('Set status code ', statusCode,
                         'in page', req.originalUrl,
                         'Catched error:', err.toString());
            logger.debug('Set status code ', statusCode, 'request body:', req.body);
            res.sendStatus(statusCode).end();
        }
    });

if (config.https) {
    var credentials = { key: config.https.private, cert: config.https.certificate };

    var httpsServer = https.createServer(credentials, app);
    httpsServer.listen(config.app.port, function () {
        logger.info('Listening ', config.app.port);
    });
} else {
    var httpServer = http.createServer(app);

    httpServer.listen(config.app.port, function () {
        logger.info('Listening ', config.app.port);
    });
}
