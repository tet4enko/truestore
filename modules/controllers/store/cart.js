'use strict';

var nodemailer = require('nodemailer');
var got = require('got');

var logger = require('../../logger');
var Order = require('../../models/order').Order;

var config = require('../../config');

var transporter = nodemailer.createTransport(
	'smtps://' + config.email.sender.address + ':' + config.email.sender.password + '@smtp.gmail.com'
);

var getOrderMailContent = function(order) {
	var text = 'Телефон: ' + order.phone + '. Тип доставки: ' + order.deliveryType + '. Комментарий: ' +
    	order.comment + '. Сумма без учета доставки: ' + order.sum + '. Товары: ' + order.items.map(function(item) {
    		return item.offerName + ' ' + item.offerColor + ' ' + item.offerCount + ' штук по цене ' + item.offerPrice + ' руб';
    	}).join(', ');

    var html = '<b>Телефон: </b>' + order.phone + '<br>' +
    	'<b>Тип доставки: </b>' + order.deliveryType + '<br>' +
    	'<b>Комментарий: </b>' + order.comment + '<br>' +
    	'<b>Сумма без учета доставки: </b>' + order.sum + ' руб<br>' +
    	'<ol><b>Товары:</b></ol>' + order.items.map(function(item) {
    		return '<li>' + item.offerName + ' ' + item.offerColor + ' ' + item.offerCount + ' штук по цене ' + item.offerPrice + ' руб' + '</li>';
    	}).join('');

    return {
    	text: text,
    	html: html
    };
};

exports.get = function(req, res, next) {
	var offers = res.locals.offers;

	res.locals = {
		env: req.env,
		data: {
			title: 'Корзина',
			offers: offers,
			pageType: 'store',
			view: 'cart'
		}
	};

	next();
};

exports.post = function(req, res, next) {
	var data = req.body;
	var order = new Order({
		sum: Number(data.totalSum),
		deliveryType: data.deliveryType,
		phone: data.phone,
		comment: data.comment,
		items: data.items
	});

	got(config.recaptcha.url, {
		method: 'POST',
		body: {
			secret: config.recaptcha.secret,
			response: data.recaptchaVal
		}
	}).then(function (apiResponse) {

        if (JSON.parse(apiResponse.body).success) {
        	order.save(function(err, order) {
				if (err) {
					res.locals = {
						status: 'error',
						error: err
					};

					next();
				} else {
					res.locals = {
						order: order,
						status: 'success'
					};

					var orderMailContent = getOrderMailContent(order);

					transporter.sendMail({
					    from: config.email.sender.address,
					    to: config.email.orders.address,
					    subject: 'Новый заказ!',
					    text: orderMailContent.text,
					    html: orderMailContent.html
					}, function(error, info){
					    if (error) {
					        console.log(error);
					    }
					    next();
					});
				}
			});
        } else {
        	res.locals = {
				error: 'recaptcha check failed',
				status: 'error'
			};

			next();
        }
    }).catch(function (err) {
    	console.log(err);
    	res.locals = {
			error: err,
			status: 'error'
		};

		next();
    });

};
