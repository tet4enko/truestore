'use strict';

var logger = require('../../logger');

exports.get = function(req, res, next) {
	var offers = res.locals.offers;
	var filters = res.locals.filters;

	console.log(req);

	res.locals = {
		env: req.env,
		data: {
			title: 'Каталог',
			offers: offers,
			filterData: filters,
			pageType: 'store',
			view: 'catalog',
			meta: {
				keywords: 'true store, true-store, трустор, купить, симферополь, крым, смартфон, meizu, apple, xiaomi, гарантия, доставка, телефон',
				description: 'Купить смартфон Apple/Xiaomi/Meizu в Крыму. Оригинальная продукция с гарантией и доставкой. Резерв и самовывоз.',
				ogtitle: 'TRUE-STORE — интернет-магазин смартфонов с самыми низкими ценами в Крыму. Оригинальная продукция. Гарантия производителя. Различные варианты доставки.',
				ogdescription:  'Купить смартфон Apple/Xiaomi/Meizu в Крыму. Оригинальная продукция с гарантией и доставкой. Резерв и самовывоз.',
				ogimage: 'https://true-store.net/_publicpics/PROMO.jpg',
				ogurl: 'https://true-store.net' + req.url
			}
		}
	};

	next();
};

exports.getOffer = function(req, res, next) {
	var offers = res.locals.offers;
	var offerTag = req.params.offerTag;
	var similar = [];
	var offerData = offers.filter(function(item) {
		return item.tag === offerTag;
	})[0];

	if (offerData) {
		var popular = offers.filter(function(item) {
			return item.popular && item._id.toString() !== offerData._id.toString();
		});

		offers.forEach(function(item) {
			if (item._id.toString() !== offerData._id.toString()
				&& item.vendor === offerData.vendor && item.model === offerData.model && similar.length < 4) {
				similar.push(item);
			}
		});

		if (similar.length < 4) {
			offers.forEach(function(item) {
				if (item._id.toString() !== offerData._id.toString()
					&& item.vendor === offerData.vendor && (similar.indexOf(item) === -1) && similar.length < 4) {
					similar.push(item);
				}
			});
		}

		res.locals = {
			env: req.env,
			data: {
				title: offerData.name,
				offers: offers,
				offerData: offerData,
				similar: similar,
				popular: popular,
				pageType: 'store',
				view: 'offer',
				meta: {
					keywords: offerData.name + ', ' + offerData.model + ', ' + offerData.name + ', ' + offerData.vendor + ' ' + offerData.model + ', недорого, гарантия, доставка',
					description: 'Купить смартфон ' + offerData.vendor + ' ' + offerData.model + '. Оригинальная продукция с гарантией и доставкой. Резерв и самовывоз.',
					ogtitle: 'TRUE-STORE — интернет-магазин смартфонов с самыми низкими ценами в Крыму. Оригинальная продукция. Гарантия производителя. Различные варианты доставки.',
					ogdescription:  'Купить смартфон ' + offerData.vendor + ' ' + offerData.model + '. Оригинальная продукция с гарантией и доставкой. Резерв и самовывоз.',
					ogimage: 'https://true-store.net/_image?offer=' + offerData._id + '&index=0',
					ogurl: 'https://true-store.net' + req.url
				}
			}
		};

		next();


	} else {
		res.redirect('/');
	}
};
