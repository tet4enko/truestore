'use strict';

var logger = require('../../logger');

exports.get = function(req, res, next) {
	var offers = res.locals.offers;

	res.locals = {
		env: req.env,
		data: {
			title: 'Доставка и оплата',
			offers: offers,
			pageType: 'store',
			view: 'delivery',
			meta: {
				keywords: 'true store, true store, доставка, самовывоз ,true-store, трустор, варианты доставки, самовывоз',
				description: 'Варианты получения товара в магазине TRUE-STORE – самовывоз, доставка курьером по Симферополю, доставка Быстрой почтой',
				ogtitle: 'TRUE-STORE — интернет-магазин смартфонов с самыми низкими ценами в Крыму. Оригинальная продукция. Гарантия производителя. Различные варианты доставки.',
				ogdescription:  'Варианты получения товара в магазине TRUE-STORE – самовывоз, доставка курьером по Симферополю, доставка Быстрой почтой',
				ogimage: 'https://true-store.net/_publicpics/PROMO.jpg',
				ogurl: 'https://true-store.net' + req.url
			}
		}
	};

	next();
};
