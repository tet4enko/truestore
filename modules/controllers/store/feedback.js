'use strict';

var nodemailer = require('nodemailer');
var got = require('got');

var logger = require('../../logger');
var config = require('../../config');

var transporter = nodemailer.createTransport(
	'smtps://' + config.email.sender.address + ':' + config.email.sender.password + '@smtp.gmail.com'
);

exports.get = function(req, res, next) {
	var offers = res.locals.offers;

	res.locals = {
		env: req.env,
		data: {
			title: 'Обратная связь',
			offers: offers,
			pageType: 'store',
			view: 'feedback',
			meta: {
				keywords: 'true store, true-store, трустор, обратная связь, отзыв, написать письмо, оставить отзыв, позвонить',
				description: 'Оставить отзыв или задать свой вопрос. Мы свяжемся с Вами в ближайшее время!',
				ogtitle: 'TRUE-STORE — интернет-магазин смартфонов с самыми низкими ценами в Крыму. Оригинальная продукция. Гарантия производителя. Различные варианты доставки.',
				ogdescription:  'Оставить отзыв или задать свой вопрос. Мы свяжемся с Вами в ближайшее время!',
				ogimage: 'https://true-store.net/_publicpics/PROMO.jpg',
				ogurl: 'https://true-store.net' + req.url
			}
		}
	};

	next();
};

exports.post = function(req, res, next) {
	got(config.recaptcha.url, {
		method: 'POST',
		body: {
			secret: config.recaptcha.secret,
			response: req.body.recaptchaVal
		}
	}).then(function (apiResponse) {

        if (JSON.parse(apiResponse.body).success) {
        	transporter.sendMail({
			    from: config.email.sender.address,
			    to: config.email.feedback.address,
			    subject: 'Обратная связь, сука!',
			    text: req.body.sender + ' ' + req.body.comment
			}, function(error, info){
			    if (error) {
			        res.locals = {
						error: error,
						status: 'error'
					};
			    } else {
			    	res.locals = {
						status: 'success'
					};
			    }

			    next();
			});
        } else {
        	res.locals = {
				error: 'recaptcha check failed',
				status: 'error'
			};

			next();
        }
    }).catch(function (err) {
    	console.log(err);
    	res.locals = {
			error: err,
			status: 'error'
		};

		next();
    });
};
