'use strict';

var logger = require('../../logger');

exports.get = function(req, res, next) {
	var offers = res.locals.offers;

	res.locals = {
		env: req.env,
		data: {
			title: 'Контакты',
			offers: offers,
			pageType: 'store',
			view: 'contacts',
			meta: {
				keywords: 'true store, true-store, трустор, контакты, телефон, email, точка, магазин, пункт, написать, позвонить, сообщить, прокомментировать',
				description: 'Контакты. Телефон – +79780241117. Email – thisistruestore@gmail.com. Пункт самовывоза – Республика Крым, Симферополь, Объездная улица, 10 (Радиорынок). Бутик № а25/a26',
				ogtitle: 'TRUE-STORE — интернет-магазин смартфонов с самыми низкими ценами в Крыму. Оригинальная продукция. Гарантия производителя. Различные варианты доставки.',
				ogdescription:  'Контакты. Телефон – +79780241117. Email – thisistruestore@gmail.com. Пункт самовывоза – Республика Крым, Симферополь, Объездная улица, 10 (Радиорынок). Бутик № а25/a26',
				ogimage: 'https://true-store.net/_publicpics/PROMO.jpg',
				ogurl: 'https://true-store.net' + req.url
			}
		}
	};

	next();
};
