'use strict';

var fs = require('fs');
var util = require('util');

var logger = require('../../logger');
var User = require('../../models/user').User;

exports.get = function(req, res, next) {
	res.locals = {
		env: req.env,
		data: {
			pageType: 'login'
		}
	};

	next();
};

exports.post = function(req, res, next) {
	
	User.authorize(req.body.username, req.body.password, function(error, user) {
		if (error) {
			res.locals = {
				status: 'error',
				error: error
			};
		} else {
			res.locals = {
				status: 'success',
				user: user
			};

			req.session.userId = user._id;
		}

		next();
	});
};

exports.logout = function(req, res, next) {
	req.session.destroy(function(error) {
		if (error) {
			console.log(error);
		} else {
			res.redirect('/_login');
		}
	});
};
