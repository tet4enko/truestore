'use strict';

var fs = require('fs');
var util = require('util');

var logger = require('../../logger');
var User = require('../../models/user').User;

exports.check = function(req, res, next) {
	var userId = req.session.userId;

	if (userId) {
		User.findOne({ _id: userId}, function(error, user) {
			if (error) {
				res.redirect('/_login/');
			} else {
				res.locals = user;
				next();
			}
		});
	} else {
		res.redirect('/_login/');
	}
};
