'use strict';

var fs = require('fs');
var util = require('util');
var xml = require('xml');
var moment = require('moment');
var formidable = require('formidable');
var crypto = require('crypto');
var touch = require('touch');

var logger = require('../../logger');

exports.get = function(req, res, next) {
	var offers = res.locals.offers;

	var head = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	var url = 'https://true-store.net';
	var sitemap = head + xml([
		{
			urlset: [{ _attr: {
				'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
				xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9',
				'xsi:schemaLocation': 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd'
			} }].concat(offers.map(function(offer) {
				return {
					url: [
						{
							loc: url + '/catalog/' + offer.tag
						},
						{
							lastmod: moment(offer.lastChange).format('YYYY-MM-DD')
						},
						{
							priority: '1.0'
						}
					]
				};
			})).concat([
				{
					url: [
						{
							loc: url + '/catalog'
						},
						{
							lastmod: moment(offers.map(function(item) { return item.lastChange; })
								.sort(function(a, b) { return b - a; })[0]).format('YYYY-MM-DD')
						},
						{
							priority: '1.0'
						}
					]
				},
				{
					url: [
						{
							loc: url + '/delivery'
						},
						{
							lastmod: '2017-02-05'
						},
						{
							priority: '1.0'
						}
					]
				},
				{
					url: [
						{
							loc: url + '/feedback'
						},
						{
							lastmod: '2017-02-05'
						},
						{
							priority: '1.0'
						}
					]
				},
				{
					url: [
						{
							loc: url + '/contacts'
						},
						{
							lastmod: '2017-02-05'
						},
						{
							priority: '1.0'
						}
					]
				}
			])
		}
	], true);

	res.set('Content-Type', 'text/xml');
	res.send(sitemap);

		
};
