'use strict';

var fs = require('fs');
var util = require('util');

var logger = require('../../logger');
var Order = require('../../models/order').Order;

exports.get = function(req, res, next) {
	Order.find()
		.then(function(results) {
			results = results
				.sort(function(a, b) {
					return b.created.valueOf() - a.created.valueOf();
				})
				.filter(function(order) {
					return order.status !== 'canceled';
				});

			res.locals = {
				env: req.env,
				data: {
					orders: results,
					pageType: 'admin',
					view: 'orders'
				}
			};

			next();
		})
		.catch(function(error) {
			res.locals = {
				env: req.env,
				data: {
					pageType: 'error'
				}
			};

			next();
		});

};

exports.getfuckingorders = function(req, res, next) {
	Order.find()
		.then(function(results) {
			results = results
				.sort(function(a, b) {
					return b.created.valueOf() - a.created.valueOf();
				})
				.filter(function(order) {
					return order.status === 'canceled' || order.status === 'closed';
				});

			res.locals = {
				env: req.env,
				data: {
					canRemove: true,
					orders: results,
					pageType: 'admin',
					view: 'orders'
				}
			};

			next();
		})
		.catch(function(error) {
			res.locals = {
				env: req.env,
				data: {
					pageType: 'error'
				}
			};

			next();
		});

};


exports.approve = function(req, res, next) {
	Order.find({ _id: req.body.id })
		.then(function(results) {
			var order = results[0];

			order.set('status', 'approved');
			order.set('lastStatusChange', new Date());

			order.save(function(err, finalOrder) {
				if (err) {
					res.locals = {
						status: 'error',
						error: err
					};
				} else {
					res.locals = {
						status: 'success',
						order: finalOrder
					};
				}

				next();
			});
		})
		.catch(function(error) {
			res.locals = {
				status: 'error',
				error: err
			};

			next();
		});

};

exports.close = function(req, res, next) {
	Order.find({ _id: req.body.id })
		.then(function(results) {
			var order = results[0];

			order.set('status', 'closed');
			order.set('lastStatusChange', new Date());

			order.save(function(err, finalOrder) {
				if (err) {
					res.locals = {
						status: 'error',
						error: err
					};
				} else {
					res.locals = {
						status: 'success',
						order: finalOrder
					};
				}

				next();
			});
		})
		.catch(function(error) {
			res.locals = {
				status: 'error',
				error: err
			};

			next();
		});
};

exports.remove = function(req, res, next) {
	Order.findOne({ _id: req.body.id }, function(err, order) {
		if (err) {
			res.locals = {
				status: 'error',
				error: error
			};
		} else {
			order.remove();

			res.locals = {
				status: 'success'
			};
		}

		next();
	});
};

exports.cancel = function(req, res, next) {
	Order.find({ _id: req.body.id })
		.then(function(results) {
			var order = results[0];

			order.set('status', 'canceled');
			order.set('lastStatusChange', new Date());

			order.save(function(err, finalOrder) {
				if (err) {
					res.locals = {
						status: 'error',
						error: err
					};
				} else {
					res.locals = {
						status: 'success',
						order: finalOrder
					};
				}

				next();
			});
		})
		.catch(function(error) {
			res.locals = {
				status: 'error',
				error: err
			};

			next();
		});
};
