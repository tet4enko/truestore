'use strict';

var fs = require('fs');
var util = require('util');
var xml = require('xml');
var moment = require('moment');
var formidable = require('formidable');
var crypto = require('crypto');
var touch = require('touch');

var logger = require('../../logger');
var Offer = require('../../models/offer').Offer;
var Sale = require('../../models/sale').Sale;

exports.get = function(req, res, next) {

	Offer.find()
		.then(function(results) {
			res.locals = {
				env: req.env,
				data: {
					offers: results.map(function(item) { return item.getSimplyfiedObject() }),
					pageType: 'admin',
					view: 'offers'
				}
			};

			next();
		})
		.catch(function(error) {
			res.locals = {
				env: req.env,
				data: {
					pageType: 'error'
				}
			};

			next();
		});
};

exports.save = function(req, res, next) {
	var form = new formidable.IncomingForm();
    form.uploadDir = 'userimages';

    form.parse(req, function(err, fields, files) {
    	if (err) {
    		res.locals = {
    			error: err,
    			status: 'error'
    		};
    	} else {
    		var picture = files.pictures;
    		var updatedOffer = {
    			name: fields.name,
    			vendor: fields.vendor,
    			model: fields.model,
    			color: fields.color,
    			description: fields.description,
    			tag: fields.tag,
    			available: fields.available === 'true',
    			price: Number(fields.price),
    			currency: fields.currency,
    			popular: fields.popular,
    			colorHex: fields.colorHex,
    			internalMemory: Number(fields.internalMemory),
    			vendorCode: fields.vendorCode,
    			lastChange: new Date()
    		};

    		if (picture) {
    			updatedOffer.pictures = [
	    			{
	    				contentType: picture.type,
	    				data: fs.readFileSync(picture.path)
	    			}
	    		];

	    		fs.unlinkSync(picture.path);
    		}

    		Offer.findByIdAndUpdate(fields._id, { $set: updatedOffer }, { new: true }, function (err, offer) {
				if (err) {
					res.locals = {
						status: 'error',
						error: err
					};
				} else {
					res.locals = {
						offer: offer.getSimplyfiedObject(),
						status: 'success'
					};
				}

				next();
			});;

    		
	    }
    });
};

exports.create = function(req, res, next) {
	var form = new formidable.IncomingForm();
    form.uploadDir = 'userimages';

    form.parse(req, function(err, fields, files) {
    	if (err) {
    		res.locals = {
    			error: err,
    			status: 'error'
    		};
    	} else {
    		var picture = files.pictures;

    		var newOffer = new Offer({
    			name: fields.name,
    			vendor: fields.vendor,
    			model: fields.model,
    			color: fields.color,
    			description: fields.description,
    			tag: fields.tag,
    			available: fields.available === 'true',
    			price: Number(fields.price),
    			currency: fields.currency,
    			opular: fields.popular,
    			colorHex: fields.colorHex,
    			internalMemory: Number(fields.internalMemory),
    			vendorCode: fields.vendorCode,
    			pictures: picture ? [
	    			{
	    				contentType: picture.type,
	    				data: fs.readFileSync(picture.path)
	    			}
	    		] : null
    		});

    		if (picture) {
    			fs.unlinkSync(picture.path);
    		}

    		newOffer.save(function (err, offer) {
				if (err) {
					res.locals = {
						status: 'error',
						error: err
					};
				} else {
					res.locals = {
						offer: offer.getSimplyfiedObject(),
						status: 'success'
					};
				}

				next();
			});
	    }
    });
};

exports.getImage = function(req, res) {
	var offerId = req.query.offer;
	var index = Number(req.query.index);

	Offer.findOne({ _id: offerId }, function(err, offer) {
		if (err) {
			res.end({
				status: 'error',
				error: error
			});
		} else {
			res.contentType(offer.pictures[index].contentType);
          	res.end(offer.pictures[index].data);
		}
	});
};

exports.remove = function(req, res, next) {
	Offer.findOne({ _id: req.body.id }, function(err, offer) {
		if (err) {
			res.locals = {
				status: 'error',
				error: error
			};
		} else {
			offer.remove();

			res.locals = {
				status: 'success'
			};
		}

		next();
	});
};

exports.getPrice = function(req, res, next) {
	Promise.all([Offer.find(), Sale.find()])
		.then(results => {
			var offers = results[0];
			var sales = results[1];

			offers = offers.map(function(offer) {
				var offerObject =  offer.getSimplyfiedObject();
				var offerSale = sales.filter(function(sale) {
					return sale.offerId === offer._id.toString();
				})[0];

				if (offerSale) {
					offerObject.oldprice = offerObject.price;
					offerObject.price = offerSale.newPrice;
				}

				return offerObject;
			});

			var head = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
			var url = 'https://true-store.net';
			var price = head + xml({
				yml_catalog: [
					{ _attr: { date: moment().format('YYYY-MM-DD HH:mm') } },
					{ 
						shop: [
							{
								name: 'true-store'
							},
							{
								company: 'МАРЧЕНКОВ ВЛАДИМИР АНАТОЛЬЕВИЧ'
							},
							{
								url: url + '/'
							},
							{
								currencies: [
									{
										currency: { _attr: { id: 'RUR', rate: 1 } }	
									}
								]
							},
							{
								categories: [
									{
										category: [ { _attr: { id: '1' } }, 'Мобильные телефоны']
									}
								]
							},
							{
								'delivery-options': [
									{
										option: { _attr: { cost: '400', days: '3' } }
									}
								]
							},
							{
								offers: offers.filter(function(offer) {
									return (offer.vendor !== 'Apple');
								}).map(function(offer) {
									var id = offer.tag;

									if (id.length > 20) {
										id = id.slice(id.length - 20);
									}

									var offerObject = {
										offer: [
											{ _attr: { id: id, available: offer.available } },
											offer.oldprice && {
												oldprice: offer.oldprice
											},
											{
												price: offer.price
											},
											{
												currencyId: offer.currency
											},
											{
												categoryId: '1'
											},
											{
												store: 'true'
											},
											{
												delivery: 'true'
											},
											{
												pickup: 'true'
											},
											{
												url: url + '/catalog/' + offer.tag
											},
											{
												vendor: offer.vendor
											},
											{
												name: offer.name
											},
											{
												param: [ { _attr: { name: 'Цвет' } }, offer.color]
											},
											{
												model: offer.model
											},
											{
												description: offer.description
											},
											{
												age: '0'
											},
											{
												manufacturer_warranty: false
											}
										].concat({
											picture: url + '/_image?offer=' + offer._id + '&index=0'
										})
									};

									if (offer.vendorCode.length) {
										offerObject.offer.push({
											vendorCode: offer.vendorCode
										});
									}

									return offerObject;
								})
							}
						]
					}
				]
			}, true);


			fs.writeFile('prices/price.xml', price, function(error) {
				if (error) {
					logger.error(error);

					res.locals = {
						status: 'error',
						error: { error: error, gg: 1 }
					};
				} else {
					res.locals = {
						status: 'success',
						path: '/_prices/price.xml'
					};
				}

				next();
			});

		})
		.catch(function(error) {
			logger.error(error);

			res.locals = {
				status: 'error',
				error: { error: error, gg: 2 }
			};

			next();
		});
		
};
