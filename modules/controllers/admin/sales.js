'use strict';

var fs = require('fs');
var util = require('util');

var logger = require('../../logger');
var Offer = require('../../models/offer').Offer;
var Sale = require('../../models/sale').Sale;

exports.get = function(req, res, next) {
	Promise.all([Offer.find(), Sale.find()])
		.then(function(results) {
			var offers = results[0];
			var sales = results[1];

			sales = sales.map(function(sale) {
				var offer = offers.filter(function(offer) {
					return (offer._id.toString() == sale.offerId.toString());
				})[0];
				var saleObject = sale.getSimplyfiedObject();

				if (!offer) {
					saleObject.offerData = null;
				} else {
					saleObject.offerData = offer;
				}

				return saleObject;
			});

			res.locals = {
				env: req.env,
				data: {
					offers: offers.map(function(item){ return item.getSimplyfiedObject() }),
					sales: sales,
					pageType: 'admin',
					view: 'sales'
				}
			};

			next();
		})
		.catch(function(error) {
			console.log(error);
			res.locals = {
				env: req.env,
				data: {
					pageType: 'error'
				}
			};

			next();
		});

};

exports.save = function(req, res, next) {
	var saleData = req.body;

	Sale.find()
		.then(function(results) {
			var existSaleOnThisOffer = false;

			results.forEach(function(sale) {
				if (sale._id.toString() !== saleData._id.toString() && sale.offerId === saleData.offerId) {
					existSaleOnThisOffer = true;
				}
			});

			if (existSaleOnThisOffer) {
				res.locals = {
					status: 'error',
					error: 'exist_sale_on_this_offer'
				};

				next();
			} else {
				Sale.findByIdAndUpdate(saleData._id, { $set: saleData }, { new: true }, function (err, sale) {
					if (err) {
						res.locals = {
							status: 'error',
							error: err
						};
					} else {
						res.locals = {
							sale: sale,
							status: 'success'
						};
					}

					next();
				});
			}
		})
		.catch(function(error) {
			res.locals = {
				status: 'error',
				error: error
			};

			next();
		});
};

exports.create = function(req, res, next) {
	var newSale = new Sale(req.body);


	Sale.find()
		.then(function(results) {
			var existSaleOnThisOffer = false;

			results.forEach(function(sale) {
				if (sale.offerId === newSale.offerId) {
					existSaleOnThisOffer = true;
				}
			});

			if (existSaleOnThisOffer) {
				res.locals = {
					status: 'error',
					error: 'exist_sale_on_this_offer'
				};

				next();
			} else {
				newSale.save(function (err, sale) {
					if (err) {
						res.locals = {
							status: 'error',
							error: err
						};
					} else {
						res.locals = {
							sale: sale,
							status: 'success'
						};
					}

					next();
				});
			}
		})
		.catch(function(error) {
			res.locals = {
				status: 'error',
				error: error
			};

			next();
		});

};

exports.remove = function(req, res, next) {
	Sale.findOne({ _id: req.body.id }, function(err, sale) {
		if (err) {
			res.locals = {
				status: 'error',
				error: error
			};
		} else {
			sale.remove();

			res.locals = {
				status: 'success'
			};
		}

		next();
	});
};
