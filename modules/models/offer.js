var mongoose = require('../mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    tag: {
        type: String,
        required: true,
        unique: true
    },
    price: {
        type: Number,
        required: true
    },
    currency: {
        type: String,
        default: 'RUR',
        required: true,
    },
    pictures: {
        type: [{ data: Buffer, contentType: String }],
        required: true
    },
    available: {
        type: Boolean,
        required: true,
        default: true
    },
    description: {
        type: String,
        required: true,
        default: ''
    },
    name: {
        type: String,
        required: true
    },
    vendor: {
        type: String,
        required: true
    },
    vendorCode: {
        type: String,
        required: true,
        default: ''
    },
    model: {
        type: String,
        required: true
    },
    color: {
        type: String
    },
    colorHex: {
        type: String
    },
    internalMemory: {
        type: Number,
        required: true,
        default: 16
    },
    popular: {
        type: Boolean,
        required: true,
        default: false
    },
    lastChange: {
        type: Date,
        required: true,
        default: Date.now
    }
});

schema.methods.getSimplyfiedObject = function() {
    return {
        _id :  this.get('_id'),
        name : this.get('name'),
        model: this.get('model'),
        vendor: this.get('vendor'),
        tag: this.get('tag'),
        description: this.get('description'),
        available: this.get('available'),
        price: this.get('price'),
        pictures: this.get('pictures').length,
        currency: this.get('currency'),
        color: this.get('color'),
        colorHex: this.get('colorHex'),
        popular: this.get('popular'),
        internalMemory: this.get('internalMemory'),
        vendorCode: this.get('vendorCode'),
        lastChange: this.get('lastChange')
    }
};

exports.Offer = mongoose.model('Offer', schema);
