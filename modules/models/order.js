var mongoose = require('../mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    phone: {
        type: String,
        required: true
    },
    comment: {
        type: String
    },
    deliveryType: {
        type: String,
        required: true,
    },
    sum: {
        type: Number,
        required: true
    },
    items: [{
        offerId: { type: String, required: true },
        offerCount: { type: Number, required: true },
        offerPrice: { type: Number, required: true },
        offerName: { type: String, required: true },
        offerColor: { type: String, required: true }
    }],
    status: {
        type: String,
        required: true,
        default: 'new'
    },
    created: {
        type: Date,
        required: true,
        default: Date.now
    },
    lastStatusChange: {
        type: Date,
        required: true,
        default: Date.now
    }
});

schema.methods.getSimplyfiedObject = function() {
    return {
        _id :  this.get('_id'),
        phone: this.get('phone'),
        comment: this.get('phone'),
        deliveryTpe: this.get('deliveryTpe'),
        sum: this.get('sum'),
        items: this.get('items'),
        lastStatusChange: this.get('lastStatusChange')
    };
};

exports.Order = mongoose.model('Order', schema);
