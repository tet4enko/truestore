var mongoose = require('../mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    offerId: {
        type: String,
        required: true
    },
    newPrice: {
        type: String,
        required: true
    }
});

schema.methods.getSimplyfiedObject = function() {
    return {
        _id :  this.get('_id'),
        offerId: this.get('offerId'),
        newPrice: this.get('newPrice')
    }
};

exports.Sale = mongoose.model('Sale', schema);
