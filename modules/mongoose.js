var mongoose = require('mongoose');
var config = require('./config');

mongoose.connect(config.mongoose.url);
mongoose.Promise = global.Promise;
mongoose.set('debug', config.mongoose.debug);

module.exports = mongoose;
