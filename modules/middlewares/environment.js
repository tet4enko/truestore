'use strict';

var config = require('../config');

module.exports = function (req, res, next) {
    req.env = {
        // TODO: переименовать
        cache_version: parseInt(Math.floor(Math.random() * 101), 10),
        env: config.app.env,
        hosts: config.hosts,
        recaptcha: config.recaptcha
    };

    next();
};
