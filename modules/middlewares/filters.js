'use strict';

var logger = require('../logger');
var Offer = require('../models/offer').Offer;
var Sale = require('../models/sale').Sale;

exports.get = function(req, res, next) {
	var offers = res.locals.offers;
	var filterData  = {
		vendor: {},
		color: {},
		internalMemory: {}
	};

	offers.forEach(function(offer) {
		if (!filterData.vendor[offer.vendor]) {
			filterData.vendor[offer.vendor] = 1;
		} else {
			filterData.vendor[offer.vendor]++;
		}

		if (!filterData.color[offer.color]) {
			filterData.color[offer.color] = 1;
		} else {
			filterData.color[offer.color]++;
		}

		if (!filterData.internalMemory[offer.internalMemory]) {
			filterData.internalMemory[offer.internalMemory] = 1;
		} else {
			filterData.internalMemory[offer.internalMemory]++;
		} 
	});

	console.log(filterData);

	res.locals.filters = filterData;

	next();
};
