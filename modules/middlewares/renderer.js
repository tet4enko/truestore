'use strict';

var path = require('path');
var config = require('../config');
var BEMTREE = require('../template/bemtree');
var BEMHTML = require('../template/bemhtml');
var logger = require('../logger');


module.exports = function (req, res, next) {
    var thisBEMTREE = BEMTREE().BEMTREE,
        thisBEMHTML = BEMHTML().BEMHTML;

    // FIXME: не надо переопределять ничего в res, переопределять можно только в res.locals
    // res.render - уже существует. И это дефолтный метод в express
    res.render = function (type, bemjson) {
        logger.info('Rendering ' + type);
        
        if (type.toLowerCase() === 'bemtree') {
            var bemtreeResult = thisBEMTREE.apply(bemjson);
            logger.info('Rendering ' + type + ': success');

            return bemtreeResult;
        } else if (type.toLowerCase() === 'bemhtml') {
            var bemhtmlResult = thisBEMHTML.apply(bemjson);
            logger.info('Rendering ' + type + ': success');

            return bemhtmlResult;
        }
    };

    next();
};
