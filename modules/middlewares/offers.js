'use strict';

var logger = require('../logger');
var Offer = require('../models/offer').Offer;
var Sale = require('../models/sale').Sale;

exports.get = function(req, res, next) {
	Promise.all([Offer.find(), Sale.find()])
		.then(function(results) {
			var offers = results[0];
			var sales = results[1];

			offers = offers.map(function(offer) {
				var offerObject =  offer.getSimplyfiedObject();
				var offerSale = sales.filter(function(sale) {
					return sale.offerId === offer._id.toString();
				})[0];

				if (offerSale) {
					offerObject.oldprice = offerObject.price;
					offerObject.price = offerSale.newPrice;
					offerObject.salePercent = Math.ceil(100 - (offerObject.price * 100 / offerObject.oldprice));
				}

				return offerObject;
			});

			res.locals.offers = offers;

			next();
		})
		.catch(function(error) {
			res.end(500);
		});
};
