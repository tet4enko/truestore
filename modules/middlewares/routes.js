'use strict';

var router = require('express').Router();
var logger = require('../logger');

var config = require('../config');
var util = require('util');

var bemtree = require('../render/bemtree');
var json = require('../render/json');
var adminOffersController = require('../controllers/admin/offers');
var adminSalesController = require('../controllers/admin/sales');
var adminOrdersController = require('../controllers/admin/orders');
var loginController = require('../controllers/auth/login');
var checkUserController = require('../controllers/auth/check');
var catalogController = require('../controllers/store/catalog');
var cartController = require('../controllers/store/cart');
var deliveryControler = require('../controllers/store/delivery');
var feedbackControler = require('../controllers/store/feedback');
var contactsControler = require('../controllers/store/contacts');
var sitemapController = require('../controllers/sitemap');

var offersMiddleware = require('./offers');
var filtersMiddleware = require('./filters');

router.get('/admin', function(req, res) {
	res.redirect('/admin/offers');
});

router.get('/_login', loginController.get, bemtree);
router.post('/_login', loginController.post, json);
router.get('/_logout', loginController.logout, json);

router.get('/admin/offers', checkUserController.check, adminOffersController.get, bemtree);
router.get('/admin/offers/price', checkUserController.check, adminOffersController.getPrice, json);
router.post('/admin/offers/save', checkUserController.check, adminOffersController.save, json);
router.post('/admin/offers/create', checkUserController.check, adminOffersController.create, json);
router.post('/admin/offers/remove', checkUserController.check, adminOffersController.remove, json);

router.get('/admin/sales', checkUserController.check, adminSalesController.get, bemtree);
router.post('/admin/sales/save', checkUserController.check, adminSalesController.save, json);
router.post('/admin/sales/create', checkUserController.check, adminSalesController.create, json);
router.post('/admin/sales/remove', checkUserController.check, adminSalesController.remove, json);

router.get('/admin/orders', checkUserController.check, adminOrdersController.get, bemtree);
router.get('/admin/fuckingorders', checkUserController.check, adminOrdersController.getfuckingorders, bemtree);
router.post('/admin/orders/approve', checkUserController.check, adminOrdersController.approve, json);
router.post('/admin/orders/close', checkUserController.check, adminOrdersController.close, json);
router.post('/admin/orders/cancel', checkUserController.check, adminOrdersController.cancel, json);
router.post('/admin/orders/remove', checkUserController.check, adminOrdersController.remove, json);

router.get('/', offersMiddleware.get, filtersMiddleware.get, catalogController.get, bemtree);
router.get('/catalog', offersMiddleware.get, filtersMiddleware.get, catalogController.get, bemtree);
router.get('/catalog/:offerTag', offersMiddleware.get, catalogController.getOffer, bemtree);
router.get('/cart', offersMiddleware.get, cartController.get, bemtree);
router.post('/cart/saveorder', cartController.post, json);
router.get('/delivery', offersMiddleware.get, deliveryControler.get, bemtree);
router.get('/feedback', offersMiddleware.get, feedbackControler.get, bemtree);
router.get('/contacts', offersMiddleware.get, contactsControler.get, bemtree);
router.post('/feedback', feedbackControler.post, json);

router.get('/_image', adminOffersController.getImage);

router.get('/sitemap.xml', offersMiddleware.get, sitemapController.get);

router.get('/ping', function(req, res) {
	logger.info('Ping success!');
    res.end('Pong!');
});

module.exports = router;
