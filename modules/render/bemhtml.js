'use strict';

module.exports = function (req, res) {
    if (req.query.__format === 'bemjson') {
        res.json(res.locals);
    } else {
        res.send(res.render('bemhtml', res.locals.bemjson));
    }
};
