'use strict';

var logger = require('../logger');

module.exports = function (req, res, next) {
    var bemjson = res.render('bemtree', {
        block: 'root',
        env: res.locals.env,
        data: res.locals.data
    });

    if (req.query.__format === 'bemjson') {
        res.json(bemjson);
    } else {
        res.send(res.render('bemhtml', bemjson));
    }
};
