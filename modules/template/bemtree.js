'use strict';

var fs = require('fs');
var vm = require('vm');
var util = require('util');
var path = require('path');
var config = require('../config');
var pathToBundle = config.hosts.staticHost.bundlesPath;

function BEMTREE() {
    var key = 'bundle',
        pathToThisBundle = util.format(pathToBundle);

    if (!BEMTREE.cache[key]) {
        var bemtree = BEMTREE.cache[key] = {
            console: console
        };

        bemtree.global = bemtree;

        vm.runInNewContext(fs.readFileSync(path.resolve(pathToThisBundle + 'common.bemtree.js'), 'UTF-8'), bemtree);
    }

    return BEMTREE.cache[key];
}
BEMTREE.cache = {};

module.exports = BEMTREE;
