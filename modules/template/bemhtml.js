'use strict';

var fs = require('fs');
var vm = require('vm');
var util = require('util');
var path = require('path');
var config = require('../config');
var pathToBundle = config.hosts.staticHost.bundlesPath;

function BEMHTML() {
    var key = 'bundle',
        pathToThisBundle = util.format(pathToBundle);

    if (!BEMHTML.cache[key]) {
        var bemhtml = BEMHTML.cache[key] = {
            console: console
        };

        bemhtml.global = bemhtml;

        vm.runInNewContext(fs.readFileSync(path.resolve(pathToThisBundle + 'common.bemhtml.js'), 'UTF-8'), bemhtml);
    }

    return BEMHTML.cache[key];
}
BEMHTML.cache = {};

module.exports = BEMHTML;
