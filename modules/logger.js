'use strict';

var winston = require('winston');
var config = require('./config').logger;
var logger = new (winston.Logger)({
	transports: config.map(function(transport) {
		return new (winston.transports[transport.name])(transport.options)
	})
});

module.exports = logger;
