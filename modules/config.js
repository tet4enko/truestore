'use strict';

var env = process.env.ENV === 'production' ? 'production' : 'development';
var config = require(__dirname + '/../configs/' + env);

module.exports = config;
