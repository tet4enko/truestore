var techs = {
        // essential
        fileProvider: require('enb/techs/file-provider'),
        fileMerge: require('enb/techs/file-merge'),

        // optimization
        borschik: require('enb-borschik/techs/borschik'),

        // css
        stylus: require('enb-stylus/techs/stylus'),

        // js
        browser: require('enb-js/techs/browser-js'),

        // bemtree
        bemtree: require('enb-bemxjst/techs/bemtree'),

        // bemhtml
        bemhtml: require('enb-bemxjst/techs/bemhtml'),

        levels: require('enb-bem-techs/techs/levels'),
        deps: require('enb-bem-techs/techs/deps'),
        files: require('enb-bem-techs/techs/files'),
        copy: require('enb/techs/file-copy')
    },
    levels = [
        { path: 'libs/bem-core/common.blocks', check: false },
        { path: 'libs/bem-core/desktop.blocks', check: false },
        { path: 'libs/bem-components/common.blocks', check: false },
        { path: 'libs/bem-components/desktop.blocks', check: false },
        { path: 'libs/bem-components/design/common.blocks', check: false },
        { path: 'libs/bem-components/design/desktop.blocks', check: false },
        { path: 'blocks/main.blocks', check: false },
        { path: 'blocks/head.blocks', check: false },
        { path: 'blocks/admin.blocks', check: false }
    ];

/**
 * Возвращает объект-технологию для `nodeConfig`
 *
 * @param {String} tech название технологии
 * @param {Object} [params] параметры для технологии
 * @returns {*[]}
 */
function use(tech, params) {
    return [
        techs[tech],
        params || {}
    ];
}

module.exports = function(config) {
    var minify = false;

    config.nodes('*.bundles/*', function(nodeConfig) {

        nodeConfig.addTechs([
            // essential
            use('levels', {
                target: '?.levels',
                levels: levels
            }),
            use('fileProvider', {
                target: '?.bemdecl.js'
            }),
            use('deps', {
                bemdeclFile: '?.bemdecl.js',
                target: '?.deps.js'
            }),
            use('files', {
                depsFile: '?.deps.js',
                filesTarget: '?.files',
                dirsTarget: '?.dirs'
            }),

            // css
            use('stylus', {
                target: '?.css',
                autoprefixer: {browsers: ['last 2 versions']}
            }),

            // bemtree
            use('bemtree', {
                target: '?.bemtree.js'
            }),

            // bemhtml
            use('bemhtml', {
                target: '?.bemhtml.js',
                forceBaseTemplates: true,
                engineOptions: { elemJsInstances: true }
            }),

            // js
            [techs.browser, { includeYM: true }],
            [techs.fileMerge, {
                target: '?.js',
                sources: ['?.browser.js', '?.bemhtml.js']
            }],

            //Minify
            use('borschik', {
                sourceTarget: '?.css',
                destTarget: '_?.css',
                freeze: true,
                minify: minify,
                tech: 'cleancss'
            }),

            use('borschik', {
                sourceTarget: '?.js',
                destTarget: '_?.js',
                freeze: false,
                minify: minify
            })

        ]);

        nodeConfig.addTargets(['?.bemtree.js', '_?.css', '_?.js']);
    });
};
