var mongoose = require('../modules/mongoose');
var async = require('async');

async.series([
    open,
    //dropDatabase,
    requireModels,
    createUsers,
    searchUsers
], function(err) {
    console.log(arguments);
    mongoose.disconnect();
    process.exit(err ? 255 : 0);
});

function open(callback) {
    mongoose.connection.on('open', callback);
}

function dropDatabase(callback) {
    var db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback) {
    require('../modules/models/user');

    async.each(Object.keys(mongoose.models), function(modelName, callback) {
      mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
}

function createUsers(callback) {
    var users = [
      {
          username: 'admin',
          email: 'tetchenko@gmail.com',
          password: '867b50ec-a27a-4f99-9422-769cb37e8d35',
          super: true
      }
    ];

    async.each(users, function(userData, callback) {
      var user = new mongoose.models.User(userData);
      user.save(callback);
    }, callback);
}

function searchUsers(callback) {
    mongoose.models.User.find().then(function(res) {
      console.log(res);
      callback();
    })
}