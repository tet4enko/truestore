var mongoose = require('../modules/mongoose');
var async = require('async');

async.series([
    open,
    //dropDatabase,
    requireModels,
    createOffers,
    searchOffers
], function(err) {
    console.log(arguments);
    mongoose.disconnect();
    process.exit(err ? 255 : 0);
});

function open(callback) {
    mongoose.connection.on('open', callback);
}

function dropDatabase(callback) {
    var db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback) {
    require('../modules/models/offer');

    async.each(Object.keys(mongoose.models), function(modelName, callback) {
      mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
}

function createOffers(callback) {
    var offers = [
        {
          tag: 'iphone5sblack',
          price: '20000',
          pictures: ['http://copyfhone.ru/wp-content/uploads/2016/02/iphone5s-spcgrey.jpg', 'http://copyfhone.ru/wp-content/uploads/2016/02/iphone5s-spcgrey.jpg', 'http://copyfhone.ru/wp-content/uploads/2016/02/iphone5s-spcgrey.jpg', 'http://copyfhone.ru/wp-content/uploads/2016/02/iphone5s-spcgrey.jpg'],
          description: 'ну телефон, блять',
          name: 'Смартфон Apple iPhone 5s 16gb black',
          vendor: 'Apple',
          model: 'iPhone 5s 16gb black'
        }
    ];

    async.each(offers, function(offerData, callback) {
      var offer = new mongoose.models.Offer(offerData);
      offer.save(callback);
    }, callback);
}

function searchOffers(callback) {
    mongoose.models.Offer.find().then(function(res) {
      console.log(res);
      callback();
    })
}